package home.ari.backend.shared

fun String.slugified(): String {
    return this.lowercase().replace("[^\\w]", "-").replace("-+", "-")
}
