package home.ari.backend.shared

interface Either<Err, Val> {
    fun isError(): Boolean

    fun <U> fold(onError: (error: Err) -> U, onValue: (value: Val) -> U): U
    fun <E, V> map(onError: (error: Err) -> E, onValue: (value: Val) -> V): Either<E,V>

    fun onSuccess(statement: (Val) -> Unit): Either<Err, Val>
    fun onFailure(statement: (Err) -> Unit): Either<Err, Val>

    companion object {
        fun <Err, Val> fail(value: Err): Error<Err, Val> {
            return Error(value)
        }

        fun <Err, Val> success(value: Val): Value<Err, Val> {
            return Value(value)
        }
    }

    data class Error<Err, Val> internal constructor(internal val value: Err): Either<Err, Val> {

        override fun isError(): Boolean {
            return true
        }

        override fun <U> fold(onFailure: (error: Err) -> U, onValue: (value: Val) -> U): U {
            return onFailure(value)
        }

        override fun <E, V> map(onError: (error: Err) -> E, onValue: (value: Val) -> V): Either<E,V> {
            return fail(onError(this.value))
        }

        override fun onSuccess(statement: (Val) -> Unit): Either<Err, Val> {
            return this
        }

        override fun onFailure(statement: (Err) -> Unit): Either<Err, Val> {
            statement(value)
            return this
        }

        override fun toString(): String {
            return "Error{$value}"
        }
    }

    data class Value<Err, Val> internal constructor(internal val value: Val): Either<Err, Val> {
        override fun isError(): Boolean {
            return false
        }

        override fun <U> fold(onError: (error: Err) -> U, onValue: (value: Val) -> U): U {
            return onValue(value)
        }


        override fun <E, V> map(onError: (error: Err) -> E, onValue: (value: Val) -> V): Either<E,V> {
            return success(onValue(this.value))
        }

        override fun onSuccess(statement: (Val) -> Unit): Either<Err, Val> {
            statement(value)
            return this
        }

        override fun onFailure(statement: (Err) -> Unit): Either<Err, Val> {
            return this
        }

        override fun toString(): String {
            return "Value{$value}"
        }

    }
}

fun <Err, Val, DE: Err, DV: Val> Either<DE, DV>.generify(): Either<Err, Val> {
    return this.fold(
            {Either.fail(it)},
            {Either.success(it)}
    )
}

fun <Err, Val, DE: Err, U> Either<DE, Val>.mapOnSuccess(onValue: (value: Val) -> Either<Err, U>): Either<Err, U> {
    return fold(
            {Either.fail(it)},
            {onValue(it).generify()}
    )
}

fun <E, U> List<Either<E, U>>.merge(): Either<E, List<U>> {
        return this.firstOrNull { it.isError() }?.map(
                {it},
                {listOf()}
        ) ?: Either.success(this.map { (it as Either.Value<E, U>).value })
}
