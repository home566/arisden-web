package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.annotations.ValueObject

@ValueObject
data class Device(val address: String) {
    /*fun url(): URL {
        return URL("http://$address:1400/status/zp")
    }*/

    /*fun controlUrl(service: String): URL {
        return URL("http://$address:1400/MediaRenderer/$service/Control")
    }*/
}
