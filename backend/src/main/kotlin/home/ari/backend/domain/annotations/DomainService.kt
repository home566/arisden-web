package home.ari.backend.domain.annotations

import java.lang.annotation.Inherited

@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class DomainService
