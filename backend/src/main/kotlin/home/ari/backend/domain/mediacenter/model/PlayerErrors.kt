package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.annotations.ValueObject


sealed class PlayerError(val message: String) {
    override fun toString(): String {
        return message
    }

    override fun equals(other: Any?): Boolean {
        return other is PlayerError && message == other.message
    }
}

@ValueObject
class RoomDoesNotHavePlayerError(room: Room): PlayerError("the room '$room' does not exist")

@ValueObject
class PlayerIsNotResponding(device: Device): PlayerError("The device '$device' is not responding")

@ValueObject
class GetSongError(device: Device, message: String): PlayerError("Cannot retrieve the song for $device because: '$message'")
