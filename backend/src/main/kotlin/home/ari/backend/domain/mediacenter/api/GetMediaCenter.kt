package home.ari.backend.domain.mediacenter.api

import home.ari.backend.domain.mediacenter.model.MediaCenter

fun interface GetMediaCenter {
    operator fun invoke(): MediaCenter
}
