package home.ari.backend.domain.mediacenter.api

import home.ari.backend.domain.mediacenter.model.Instructions
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.PlayerError
import home.ari.backend.shared.Either

interface ConfigureMediaCenter {
    operator fun invoke(instructions: Instructions): Either<PlayerError, List<Player>>
}
