package home.ari.backend.domain.flatplan.model

data class Position(val x: Double, val y: Double) {
    companion object {
        val NULL = Position(Double.MIN_VALUE, Double.MAX_VALUE)
    }
}
