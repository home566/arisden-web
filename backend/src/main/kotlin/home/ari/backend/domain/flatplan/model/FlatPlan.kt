package home.ari.backend.domain.flatplan.model

import home.ari.backend.domain.annotations.Aggregate

@Aggregate
data class FlatPlan(val name: String, val rooms: List<Room>) {
}
