package home.ari.backend.domain.mediacenter.model.media

enum class PlayingState {
    Playing,
    Stopped,
    Unknown
}
