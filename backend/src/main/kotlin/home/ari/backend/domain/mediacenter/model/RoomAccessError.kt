package home.ari.backend.domain.mediacenter.model


class RoomAccessError(val device: Device) {
    override fun toString(): String {
        return "Cannot find a room for the device '$device'"
    }
}
