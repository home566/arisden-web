package home.ari.backend.domain.flatplan.services

import home.ari.backend.domain.annotations.DomainService
import home.ari.backend.domain.flatplan.api.GetPlan
import home.ari.backend.domain.flatplan.model.FlatPlan
import home.ari.backend.domain.flatplan.spi.Plans
import home.ari.backend.domain.flatplan.spi.RoomPositions

@DomainService
class PlanGetter(val roomPositions: RoomPositions): GetPlan {
    override infix fun named(name: String): FlatPlan {
        val rooms = roomPositions `in`(name)
        return FlatPlan(name, rooms)
    }
}
