package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.annotations.ValueObject

@ValueObject
data class Instruction(val device: Device, val command: Action) {
    infix fun `is overridden by`(other: Instruction): Boolean {
        return device == other.device &&
                command `is same kind of` other.command
    }
}
