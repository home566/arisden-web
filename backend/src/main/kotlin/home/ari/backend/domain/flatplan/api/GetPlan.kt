package home.ari.backend.domain.flatplan.api

import home.ari.backend.domain.flatplan.model.FlatPlan

interface GetPlan {
    infix fun named(name: String): FlatPlan
}
