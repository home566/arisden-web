package home.ari.backend.domain.mediacenter.services

import home.ari.backend.domain.annotations.DomainService
import home.ari.backend.domain.mediacenter.api.ConfigureMediaCenter
import home.ari.backend.domain.mediacenter.api.GetMediaCenter
import home.ari.backend.domain.mediacenter.model.Instructions
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.PlayerError
import home.ari.backend.domain.mediacenter.spi.Players
import home.ari.backend.shared.Either
import home.ari.backend.shared.mapOnSuccess


@DomainService
class SonosInstallation(private val players: Players): GetMediaCenter, ConfigureMediaCenter {
    override fun invoke(): MediaCenter {
        return MediaCenter(players.getAll())
    }

    override fun invoke(instructions: Instructions): Either<PlayerError, List<Player>> {
        return players.configure(instructions)
    }
}


