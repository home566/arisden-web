package home.ari.backend.domain.mediacenter.model

import home.ari.backend.shared.slugified

data class Room(val name: String) {

    val id = name.slugified()

    infix fun `is`(name: String): Boolean {
        return name.equals(this.name, ignoreCase = true)
    }
}
