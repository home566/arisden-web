package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.annotations.ValueObject

@ValueObject
data class Instructions private constructor(val pending: List<Instruction>): Iterable<Instruction> {

    constructor(): this(emptyList())

    override fun iterator(): Iterator<Instruction> {
        return pending.iterator()
    }

    operator fun plus(instruction: Instruction): Instructions {
        return Instructions(this.pending.filter { !(it `is overridden by` instruction) } + instruction)
    }
}
