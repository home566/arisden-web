package home.ari.backend.domain.flatplan.model

import home.ari.backend.domain.annotations.Entity

@Entity
class Room(val name: String, val angles: List<Position>, val playerPosition: Position) {
    override fun equals(other: Any?): Boolean {
        return other is Room && name.equals(other.name, ignoreCase = true)
    }
}
