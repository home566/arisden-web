package home.ari.backend.domain.flatplan.spi

import home.ari.backend.domain.flatplan.model.Room

interface RoomPositions {
    infix fun `in`(plan: String): List<Room>
}
