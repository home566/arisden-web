package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.shared.Either


class Player(
    val room: Room,
    val device: Device,
    val song: Either<PlayerError, Song>,
    val state: Either<PlayerError, PlayingState>
) {
    constructor(room: Room,
                device: Device,
                song: Song,
                state: PlayingState): this(room, device, Either.success(song), Either.success(state))

    fun readyToPlay(): Boolean {
        return state.fold(
                { false },
                { it == PlayingState.Stopped }
        )
    }

    override fun equals(other: Any?): Boolean {
        return other is Player && room == other.room && device == other.device
    }
}
