package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.annotations.Aggregate
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.shared.Either


@Aggregate
class MediaCenter constructor(
    val players: List<Player>) {

    private var instructions =  Instructions()

    fun playerOf(roomName: Room): Either<RoomDoesNotHavePlayerError, Player> {
        val room = players.firstOrNull { player -> player.room == roomName }
            ?: return Either.fail(RoomDoesNotHavePlayerError(roomName))
        return Either.success(room)
    }

    fun `song playing in`(roomName: Room): Either<PlayerError, Song> {
        val room = players.firstOrNull { player -> player.room == roomName }
            ?: return Either.fail(RoomDoesNotHavePlayerError(roomName))
        return room.song.fold( { Either.success(Song.nothing()) },
                { Either.success(it) }
        )
    }

    infix fun `has room named`(name: String): Boolean {
        return players.firstOrNull { it.room `is` name } != null
    }

    fun play(room: Room) = configure(room, Action.Play)
    fun stop(room: Room) = configure(room, Action.Stop)

    fun takePendingInstructions(): Instructions {
        val i = instructions
        this.instructions = Instructions()
        return i

    }

    private fun configure(room: Room, action: Action): Either<PlayerError, MediaCenter> {
        return playerOf(room).fold (
                { Either.fail(it) },
                {
                    instructions += Instruction(it.device, action)
                    Either.success(this)
                })
    }
}
