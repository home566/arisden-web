package home.ari.backend.domain.mediacenter.model.media

data class Song(val artist: String, val album: String, val title: String, val source: Source) {
    companion object {
        fun nothing(): Song {
            return Song ("", "", "", Source.None)
        }
    }
}


enum class Source {
    Spotify,
    RadioParadise,
    TV,
    None,
    Unknown
}


