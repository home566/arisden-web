package home.ari.backend.domain.mediacenter.spi

import home.ari.backend.domain.mediacenter.model.Instructions
import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.PlayerError
import home.ari.backend.shared.Either
import java.util.*


typealias UpdatedPlayers = List<Player>

interface Players {
    fun getAll(): List<Player>
    fun configure(instructions: Instructions): Either<PlayerError, UpdatedPlayers>
}
