package home.ari.backend.domain.mediacenter.model

enum class Action {
    Play,
    Stop;

    infix fun `is same kind of`(other: Action): Boolean {
        return true
    }


    companion object {
        fun asAction(s: String): Action {
            return values().first { it.toString().equals(s, ignoreCase = true) }
        }
    }

}
