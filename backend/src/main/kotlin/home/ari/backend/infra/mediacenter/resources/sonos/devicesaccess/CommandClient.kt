package home.ari.backend.infra.mediacenter.resources.sonos.devicesaccess


import home.ari.backend.domain.mediacenter.model.Device
import home.ari.backend.infra.mediacenter.resources.request.RestClient
import home.ari.backend.infra.mediacenter.resources.request.post
import org.apache.commons.text.StringEscapeUtils
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Repository
import java.net.URL

@Repository
class CommandClient(val restClient: RestClient) {

    fun defaultBody(service: String, command: String): String {
        return "<u:$command xmlns:u=\"urn:schemas-upnp-org:service:$service:1\"><InstanceID>0</InstanceID><Channel>Master</Channel></u:$command>"
    }

    fun <T> send(device: Device, command: String, service: String, debug: Boolean, body: String = defaultBody(service, command), parser: (String) -> T): T {
        val headers = buildHeaders()
        headers.add("SOAPACTION", "urn:schemas-upnp-org:service:AVTransport:1#$command")
        headers.contentType = MediaType.APPLICATION_XML

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body>$body</s:Body></s:Envelope>
        """.trimIndent()
        val request = HttpEntity(payload, headers)

        val content = restClient.post(URL("http://${device.address}:1400/MediaRenderer/$service/Control"), request, debug=debug) ?: ""
        return parser(StringEscapeUtils.unescapeXml(content))
    }


    private fun buildHeaders(): HttpHeaders {
        val httpHeaders = HttpHeaders()
        return httpHeaders
    }
}



/*
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetPositionInfo></s:Body></s:Envelope>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetVolume xmlns:u="urn:schemas-upnp-org:service:RenderingControl:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetVolume></s:Body></s:Envelope>

<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:Play xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><Speed>1</Speed><InstanceID>0</InstanceID></u:Play></s:Body></s:Envelope>
*/
