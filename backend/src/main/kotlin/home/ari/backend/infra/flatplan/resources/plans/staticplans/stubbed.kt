package home.ari.backend.infra.flatplan.resources.plans.staticplans

import home.ari.backend.infra.flatplan.resources.plans.StaticPlan
import home.ari.backend.infra.flatplan.resources.plans.staticplans.PlanConfiguration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import kotlin.properties.Delegates
import home.ari.backend.domain.flatplan.model.Room as DomainRoom

@StaticPlan
@Profile("stubbed")
@Primary
class  Stubbed: PlanConfiguration() {

    init {
        room {
            p(0, 0)
            p(54, 0)
            p(54, 83)
            p(31, 83)
            p(31, 62)
            p(0, 62)
            name = "Living Room"
            `player at`(4, 30)
        }

        room {
            p(0, 63)
            p(30, 63)
            p(30, 83)
            p(0, 83)
            name = "Kitchen"
        }

        room {
            p(55, 0)
            p(98, 0)
            p(98, 67)
            p(81, 67)
            p(81, 48)
            p(55, 48)
            name = "Library"
            `player at`(59, 20)
        }

        room {
            p(98, 68)
            p(140, 68)
            p(140, 104)
            p(129, 104)
            p(129, 112)
            p(98, 112)
            name = "Bedroom"
            `player at`(102, 80)
        }

        room {
            p(59, 86)
            p(97, 86)
            p(97, 112)
            p(59, 112)
            name = "Bathroom"
            `player at`(63, 99)
        }

        room {
            p(55, 49)
            p(80, 49)
            p(80, 67)
            p(55, 67)
            name = "WC"
        }

        room {
            p(55, 68)
            p(97, 68)
            p(97, 85)
            p(59, 85)
            p(59, 83)
            p(55, 83)
            name = "Hallway"
        }
    }
}
