package home.ari.backend.infra.configuration


import home.ari.backend.domain.annotations.DomainService
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.FilterType

@Configuration
@ComponentScan(
    basePackages = ["home.ari"],
    includeFilters = [ComponentScan.Filter(type = FilterType.ANNOTATION, value = [DomainService::class])]
)
class DomainInjectionConfiguration
