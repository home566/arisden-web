package home.ari.backend.infra.mediacenter.resources

import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.infra.mediacenter.resources.request.RestClient
import home.ari.backend.infra.mediacenter.resources.request.get
import home.ari.backend.shared.Either
import org.json.JSONException
import org.json.JSONObject
import java.net.URL

class NowPlayingOnRadioParadise(val restClient: RestClient) {

    private fun getSong(retry: Boolean = true): JSONObject? {
        try {
            val response =
                JSONObject(restClient.get<String>(URL("https://api.radioparadise.com/api/get_block?info=true")))
            val currentEventId = response.getString("current_event_id")
            val songs = response.getJSONObject("song")
            val keys = songs.keys().asSequence().toList()
            val tracks = keys.map { i -> songs.getJSONObject("$i") }

            val currentSong = tracks
                .firstOrNull { song -> song.getString("event") == currentEventId }
            if (currentSong == null) {
                if (retry) {
                    Thread.sleep(1000)
                    return getSong(retry = false)
                }
                return null
            }

            return currentSong
        } catch (e: JSONException) {
            println(e.message)
            return null
        }
    }

    operator fun invoke(): Either<RPError, Song> {
        val currentSong = getSong() ?: return Either.fail(RPError())
        return Either.success(Song(
                currentSong.getString("artist"),
                currentSong.optString("album", ""),
                currentSong.getString("title"),
                Source.RadioParadise
        ))
    }
}
