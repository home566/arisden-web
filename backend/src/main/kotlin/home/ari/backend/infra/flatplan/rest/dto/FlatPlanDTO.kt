package home.ari.backend.infra.flatplan.rest.dto

data class FlatPlanDTO(val rooms: List<RoomDTO>) {
}
