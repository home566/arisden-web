package home.ari.backend.infra.flatplan.rest.controllers

import home.ari.backend.domain.flatplan.api.GetPlan
import home.ari.backend.domain.flatplan.model.FlatPlan
import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.domain.mediacenter.api.GetMediaCenter
import home.ari.backend.infra.flatplan.rest.dto.FlatPlanDTO
import home.ari.backend.infra.flatplan.rest.dto.RoomDTO
import org.springframework.hateoas.server.EntityLinks
import org.springframework.hateoas.server.ExposesResourceFor
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/flat-plan")
@CrossOrigin("*")
@ExposesResourceFor(FlatPlanDTO::class)
class FlatPlanController(private val getPlan: GetPlan, private val getMediaCenter: GetMediaCenter, private val entityLinks: EntityLinks) {
    @GetMapping("/{name}")
    fun get(@PathVariable("name") name: String): ResponseEntity<FlatPlanDTO> {
        return ResponseEntity.ok(getPlan.named(name).toDTO())
    }

    fun FlatPlan.toDTO(): FlatPlanDTO {
        return FlatPlanDTO(this.rooms.map { it.toDTO() })
    }

    private fun Room.toDTO(): RoomDTO {
        val url = if(getMediaCenter().`has room named`(name)) "http://localhost:8080/mediacenter/$name"
                  else null
        return RoomDTO(this.name, this.angles,
                url,
                this.playerPosition)
    }
}

