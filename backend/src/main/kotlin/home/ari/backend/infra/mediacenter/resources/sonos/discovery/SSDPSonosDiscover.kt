package home.ari.backend.infra.mediacenter.resources.sonos.discovery


import home.ari.backend.domain.mediacenter.model.Device
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.SocketTimeoutException

@Component
class SonosDiscoveryBySSDP: SonosDiscovery {

    override fun getAll(): List<Device> {
        val msearch =
            """
            M-SEARCH * HTTP/1.1\nHost: 239.255.255.250:1900
            Man: "ssdp:discover"
            ST: urn:schemas-upnp-org:device:ZonePlayer:1
            
            """.trimIndent()
        val receiveData = ByteArray(1024)
        val sendData = msearch.toByteArray()
        val sendPacket = DatagramPacket(
            sendData,
            sendData.size,
            InetAddress.getByName("239.255.255.250"),
            1900
        )

        val clientSocket = DatagramSocket()

        clientSocket.soTimeout = 1000
        clientSocket.send(sendPacket)

        val devices = mutableListOf<Device>()
        while (true) {
            try {
                val receivePacket = DatagramPacket(receiveData, receiveData.size)
                clientSocket.receive(receivePacket)
                devices.add(Device(receivePacket.address.hostAddress))
            } catch (e: SocketTimeoutException) {
                break
            }
        }
        clientSocket.close()

        return devices
    }
}
