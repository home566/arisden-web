package home.ari.backend.infra.flatplan.resources.plans.staticplans

import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.infra.flatplan.resources.plans.RoomBuilder

abstract class PlanConfiguration {
    private val roomBuilders = mutableListOf<RoomBuilder>()

    val rooms: List<Room>
        get() = roomBuilders.map { it.build() }

    fun room(f: RoomBuilder.() -> Unit) {
        val r = RoomBuilder()
        r.f()
        roomBuilders.add(r)
    }
}
