package home.ari.backend.infra.mediacenter.resources.model.sonos

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "ZPSupportInfo")
class ZPSupportInfo {
    @JacksonXmlProperty(localName = "ZPInfo")
    var info: DeviceInfo = DeviceInfo()

    val name: String
    get() = info.name
}
