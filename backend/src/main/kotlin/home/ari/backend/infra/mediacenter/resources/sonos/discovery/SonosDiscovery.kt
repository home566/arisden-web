package home.ari.backend.infra.mediacenter.resources.sonos.discovery

import home.ari.backend.domain.mediacenter.model.Device


interface SonosDiscovery {
    fun getAll(): List<Device>
}
