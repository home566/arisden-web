package home.ari.backend.infra.configuration

import org.springframework.context.annotation.Bean
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Bean
fun corsConfigurationSource(): CorsConfigurationSource {
    val configuration = CorsConfiguration()
    configuration.addAllowedHeader("*")
    configuration.allowedMethods = listOf("GET", "PUT", "HEAD", "OPTIONS", "POST", "DELETE", "PATCH")
    configuration.addAllowedOrigin("*")

    val source = UrlBasedCorsConfigurationSource()
    source.registerCorsConfiguration("/**", configuration)
    return source
}
