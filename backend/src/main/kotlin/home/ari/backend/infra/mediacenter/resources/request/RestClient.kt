package home.ari.backend.infra.mediacenter.resources.request

import org.springframework.http.HttpEntity
import java.net.URL
import kotlin.reflect.KClass


interface RestClient {
    fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T?
    fun <T: Any> get(url: URL, payload: String, type: KClass<T>, debug: Boolean): T?
}

inline fun <reified T : Any> RestClient.get(url: URL, payload: String = "", debug: Boolean = false): T? = get(url, payload, T::class, debug)
inline fun <reified T : Any> RestClient.post(url: URL, request: HttpEntity<String>, debug: Boolean = false): T? = post(
        url,
        request,
        T::class,
        debug
)

