package home.ari.backend.infra.mediacenter.rest.controllers

import home.ari.backend.domain.mediacenter.api.ConfigureMediaCenter
import home.ari.backend.domain.mediacenter.api.GetMediaCenter
import home.ari.backend.domain.mediacenter.model.Action
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.Room
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.spi.Players
import home.ari.backend.infra.mediacenter.rest.dto.MediaCenterDTO
import home.ari.backend.infra.mediacenter.rest.dto.PlayerDTO
import org.springframework.hateoas.server.EntityLinks
import org.springframework.hateoas.server.ExposesResourceFor
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("/mediacenter")
@CrossOrigin("*")
@ExposesResourceFor(MediaCenterDTO::class)
class MediaCenterController(val getMediaCenter: GetMediaCenter,
                            val configureMediaCenter: ConfigureMediaCenter,
                            @Suppress("SpringJavaInjectionPointsAutowiringInspection") private val entityLinks: EntityLinks
) {

    @GetMapping
    fun get(): ResponseEntity<MediaCenterDTO> {
        return ResponseEntity.ok(getMediaCenter().toDTO())
    }

    @GetMapping("/{room}")
    fun get(@PathVariable("room") room: String): ResponseEntity<*> {
        return getMediaCenter().playerOf(Room(room)).fold(
                {error -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(error)},
                {player -> ResponseEntity.ok(player.toDTO())}
        )
    }

    @PostMapping("/{room}/{action:play|stop}")
    fun play(@PathVariable("room") room: String, @PathVariable("action") requestedAction: String): ResponseEntity<*> {

        val action = Action.asAction(requestedAction)

        return when(action) {
            Action.Play -> getMediaCenter().play(Room(room))
            Action.Stop -> getMediaCenter().stop(Room(room))
        }.fold(
                { ResponseEntity.status(HttpStatus.BAD_REQUEST).body(it) },
                {
                    mediaCenter ->
                    configureMediaCenter(mediaCenter.takePendingInstructions()).fold(
                            { ResponseEntity.status(HttpStatus.BAD_REQUEST).body(it)},
                            { updatedPlayers -> ResponseEntity.ok(updatedPlayers.map{it.toDTO()}) }
                    )
                }
        )
    }


    @GetMapping("/now-playing/{room}")
    fun nowPlaying(@PathVariable("room") room: String): ResponseEntity<*> {
        return getMediaCenter().`song playing in`(Room(room)).fold(
                {error -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(error)},
                {song -> ResponseEntity.ok(song)}
        )
    }

    fun Player.toDTO(): PlayerDTO {
        val (song, state) =
            song.fold({Song.nothing()}, {it}) and this.state.fold({ PlayingState.Unknown }, {it})

        val dto = PlayerDTO(room.name, song, device.address, state.toString())
            .addIf(this.readyToPlay()) {
                entityLinks.linkForItemResource(MediaCenterDTO::class.java, this.room.name).slash("play").withRel("play")
            }
            .addIf(!this.readyToPlay()) {
                entityLinks.linkForItemResource(MediaCenterDTO::class.java, this.room.name).slash("stop").withRel("stop")
            }
        return dto
    }

    fun MediaCenter.toDTO(): MediaCenterDTO {
        return MediaCenterDTO(players.map { it.toDTO() })
    }
}

infix fun <F, S> F.and(other: S): Pair<F, S> {
    return Pair(this, other)
}
