package home.ari.backend.infra.mediacenter.resources

object RPError {
    operator fun invoke(): RPError {
        return this
    }

    const val message = "Cannot retrieve Radio Paradise metadata"
}
