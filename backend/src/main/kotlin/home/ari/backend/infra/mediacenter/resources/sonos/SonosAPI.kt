package home.ari.backend.infra.mediacenter.resources.sonos

import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.domain.mediacenter.spi.Players
import home.ari.backend.domain.mediacenter.spi.UpdatedPlayers
import home.ari.backend.infra.mediacenter.resources.NowPlayingOnRadioParadise
import home.ari.backend.shared.Either
import home.ari.backend.infra.mediacenter.resources.sonos.devicesaccess.CommandClient
import home.ari.backend.infra.mediacenter.resources.sonos.discovery.SonosDiscovery
import home.ari.backend.infra.mediacenter.resources.model.sonos.ZPSupportInfo
import home.ari.backend.infra.mediacenter.resources.request.RestClient
import home.ari.backend.infra.mediacenter.resources.request.get
import home.ari.backend.shared.mapOnSuccess
import home.ari.backend.shared.merge

import org.springframework.stereotype.Component
import java.net.URL


@Component
class SonosAPI(
    private val restClient: RestClient,
    private val discovery: SonosDiscovery,
): Players {
    private val radioParadiseCurrentTrack = NowPlayingOnRadioParadise(restClient)
    private val commandClient = CommandClient(restClient)

    private fun cannotAccessToDevice(device: Device): Either<RoomAccessError, Room> {
        val error = RoomAccessError(device)
        return Either.fail(error)
    }

    fun getRoomOf(device: Device): Either<RoomAccessError, Room> {
        val info = getInfo(device) ?: return cannotAccessToDevice(device)
        return Either.success(Room(info.name))
    }

    private fun getPlayerOf(device: Device): Player? {
        return getRoomOf(device).fold(
                {
                    null
                },
                { room ->
                    Player(room, device, device.getTrack(), device.getState())
                })
    }

    override fun getAll(): List<Player> {
        return discovery.getAll().mapNotNull { device -> getPlayerOf(device) }
    }

    override fun configure(instructions: Instructions): Either<PlayerError, UpdatedPlayers> {
        return instructions.map { (device, command) ->
                when(command) {
                    Action.Play -> device.play()
                    Action.Stop -> device.stop()
                }.mapOnSuccess {
                    val player = getPlayerOf(device)
                    if (player != null)
                        Either.success(player)
                    else
                        Either.fail(PlayerIsNotResponding(device))
                }
        }.merge()
    }

    private fun getInfo(device: Device): ZPSupportInfo? {
        return restClient.get(URL("http://${device.address}:1400/status/zp"))
    }

    private fun Device.getTrack() = commandClient.send(
            this,
            "GetPositionInfo",
            "AVTransport",
            false
    ) { s -> this.extractTrack(s) }

    private fun Device.getState() = commandClient.send(
            this,
            "GetTransportInfo",
            "AVTransport",
            false
    ) { s -> this.extractState(s) }

    private fun Device.play(): Either<PlayerError, PlayingState> = commandClient.send(this, "Play", "AVTransport",
            body="<u:Play xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\"><Speed>1</Speed><InstanceID>0</InstanceID></u:Play>",
            debug=true) { _: String ->
        Either.success(
                PlayingState.Playing
        )
    }

    private fun Device.stop(): Either<PlayerError, PlayingState> = commandClient.send(this, "Stop", "AVTransport",
            body="<u:Stop xmlns:u=\"urn:schemas-upnp-org:service:AVTransport:1\"><Speed>1</Speed><InstanceID>0</InstanceID></u:Stop>",
            debug=true) { _: String ->
        Either.success(
                PlayingState.Stopped
        )
    }

    fun Device.extractTrack(content: String): Either<PlayerError, Song> {
        val artist = content.extract("dc:creator")
        val album = content.extract("upnp:album")
        val title = content.extract("dc:title")
        val trackURI = content.extract("TrackURI")

        if (trackURI.contains("radioparadise.com")) {
            return radioParadiseCurrentTrack().map(
                    { GetSongError(this, it.message) },
                    {it}
            )
        }
        if (listOf(artist, album, title).`are not empty`())
            return Either.success(Song.nothing())

        val source = Source.Spotify

        return Either.success(Song(artist, album, title, source))
    }

    fun Device.extractState(content: String): Either<PlayerError, PlayingState> {
        val state = content.extract("CurrentTransportState")
        if (state.isBlank()) {
            return Either.fail(PlayerIsNotResponding(this))
        }
        return Either.success(state.toEnum())
    }
}


private fun List<String>.`are not empty`(): Boolean {
    return this.all { it.isBlank() }
}

fun String.extract(tag: String): String {
    val rx = "<$tag>(.+)</$tag>".toRegex()
    val match = rx.find(this)

    if (match != null) {
        return match.groupValues[1]
    }
    return ""
}



fun String.toEnum(): PlayingState {
    if (this.uppercase() == "PLAYING")
        return PlayingState.Playing
    return PlayingState.Stopped
}
