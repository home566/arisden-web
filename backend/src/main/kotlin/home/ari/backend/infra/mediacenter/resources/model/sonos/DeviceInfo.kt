package home.ari.backend.infra.mediacenter.resources.model.sonos

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty


class DeviceInfo {
    @JacksonXmlProperty(localName = "ZoneName")
    var name: String = ""
}


/*
<?xml version="1.0" ?>
<?xml-stylesheet type="text/xsl" href="/xml/review.xsl"?>
<ZPSupportInfo>
    <ZPInfo>
        <ZoneName>Platine</ZoneName>
        <ZoneIcon>x-rincon-roomicon:living</ZoneIcon>
        <Configuration>1</Configuration>
        <LocalUID>RINCON_5CAAFDE9897A01400</LocalUID>
        <SerialNumber>5C-AA-FD-E9-89-7A:E</SerialNumber>
        <SoftwareVersion>65.1-21040</SoftwareVersion>
        <BuildType>release</BuildType>
        <SWGen>2</SWGen>
        <SoftwareDate>2021-09-04 12:59:16.366511</SoftwareDate>
        <SoftwareScm>c6f41fd2e</SoftwareScm>
        <HHSwgenState>allow:2</HHSwgenState>
        <MinCompatibleVersion>64.0-00000</MinCompatibleVersion>
        <LegacyCompatibleVersion>64.0-00000</LegacyCompatibleVersion>
        <HardwareVersion>1.17.3.1-2.1</HardwareVersion>
        <DspVersion>0.25.3</DspVersion>
        <SeriesID>C100</SeriesID>
        <HwFlags>0x14</HwFlags>
        <HwFeatures>0x20</HwFeatures>
        <Variant>1</Variant>
        <GeneralFlags>0x0</GeneralFlags>
        <IPAddress>192.168.1.15</IPAddress>
        <MACAddress>5C:AA:FD:E9:89:7A</MACAddress>
        <Copyright>© 2003-2021, Sonos, Inc. All rights reserved.</Copyright>
        <ExtraInfo>OTP: 1.1.1(1-17-3-2.1)</ExtraInfo>
        <HTAudioInCode>0</HTAudioInCode>
        <IdxTrk></IdxTrk>
        <MDP2Ver>4</MDP2Ver>
        <MDP3Ver>0</MDP3Ver>
        <HouseholdControlID>Sonos_VnSprBwjUPErWMc8gtnYE9CHTO.SaRvHf-884MORszztWLg</HouseholdControlID>
    </ZPInfo>
    <!-- SDT: 2 ms -->
</ZPSupportInfo>
 */
