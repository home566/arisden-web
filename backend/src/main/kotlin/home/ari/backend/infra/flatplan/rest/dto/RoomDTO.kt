package home.ari.backend.infra.flatplan.rest.dto

import home.ari.backend.domain.flatplan.model.Position

data class RoomDTO(val name: String, val angles: List<Position>, val playerInformationUrl: String?, val playerPosition: Position) {
}
