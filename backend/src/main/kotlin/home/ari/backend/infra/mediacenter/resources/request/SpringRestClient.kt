package home.ari.backend.infra.mediacenter.resources.request

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import java.net.URL
import kotlin.reflect.KClass

@Repository
class SpringRestClient(
    restTemplateBuilder: RestTemplateBuilder,
): RestClient {
    private var httpClient =  HttpClient(restTemplateBuilder.build())

    override fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T? {
        return httpClient.post(url, request, type, debug)
    }

    override fun <T: Any> get(url: URL, payload: String, type: KClass<T>, debug: Boolean): T? {
        return httpClient.get(url, payload, type)
    }
}



private class HttpClient(
    private val restTemplate: RestTemplate
) {
    private fun buildHeaders(): HttpHeaders {
        val httpHeaders = HttpHeaders()
        return httpHeaders
    }

    fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T? {
        //val headers = buildHeaders()
        //headers.contentType = MediaType.APPLICATION_JSON
        if (debug) {
            println(url.toString())
            println(request.headers.map { it.toString() }.joinToString("\n") { "    - $it" })
            println(request.body)
        }

        println(url)
        return restTemplate.exchange(url.toString(), HttpMethod.POST, request, type.java).body
    }

    fun <T: Any> get(url: URL, payload: String, type: KClass<T>): T? {
        val headers = buildHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity<Class<T>>(headers)
        return restTemplate.exchange(url.toString(), HttpMethod.GET, request, type.java, payload).body
    }
}

/*
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:Play xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><Speed>1</Speed><InstanceID>0</InstanceID></u:Play></s:Body></s:Envelope>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:Play xmlns:u="urn:schemas-upnp-org:service:GetTransportInfo:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:Play></s:Body></s:Envelope>

 */
