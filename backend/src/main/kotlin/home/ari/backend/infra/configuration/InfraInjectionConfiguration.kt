package home.ari.backend.infra.configuration

import home.ari.backend.infra.flatplan.resources.plans.StaticPlan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.FilterType

@Configuration
@ComponentScan(
    basePackages = ["home.ari"],
    includeFilters = [ComponentScan.Filter(type = FilterType.ANNOTATION, value = [StaticPlan::class])]
)
class InfraInjectionConfiguration
