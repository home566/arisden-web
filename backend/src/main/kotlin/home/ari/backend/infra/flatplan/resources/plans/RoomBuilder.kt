package home.ari.backend.infra.flatplan.resources.plans

import home.ari.backend.domain.flatplan.model.Position
import home.ari.backend.domain.flatplan.model.Room

data class RoomBuilder(val angles: MutableList<Position>) {
    var name: String = ""

    var playerPosition: Position = Position.NULL

    constructor(): this(mutableListOf<Position>())

    fun p(x: Int, y: Int): RoomBuilder {
        angles.add(Position(x.toDouble(), y.toDouble()))
        return this
    }

    fun `player at`(x: Int, y: Int): RoomBuilder {
        playerPosition = Position(x.toDouble(), y.toDouble())
        return this
    }

    fun build(): Room {
        return Room(name, angles, playerPosition)
    }
}
