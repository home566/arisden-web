package home.ari.backend.infra.flatplan.resources

import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.domain.flatplan.spi.Plans
import home.ari.backend.domain.flatplan.spi.RoomPositions
import home.ari.backend.infra.flatplan.resources.plans.staticplans.PlanConfiguration
import org.springframework.stereotype.Repository


@Repository
class ArisPlans(val plan: PlanConfiguration): Plans, RoomPositions {
    override fun `in`(name: String): List<Room> {
        return plan.rooms
    }
}
