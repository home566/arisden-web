package home.ari.backend.infra.mediacenter.rest.dto

import home.ari.backend.domain.mediacenter.model.media.Song
import org.springframework.hateoas.RepresentationModel

data class PlayerDTO(val room: String, val song: Song, val address: String, val state: String): RepresentationModel<PlayerDTO>()
