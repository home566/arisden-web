package home.ari.backend.infra.doubles

import home.ari.backend.infra.mediacenter.resources.request.RestClient
import org.springframework.http.HttpEntity
import java.net.URL
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
class RestClientDouble: RestClient {

    val responses = mutableListOf<Any?>()

    val urlMappeResponses = mutableMapOf<String, Any?>()

    inner class UrlMappeResponsesBuilder(val url: String) {
        infix fun willReturn(value: Any?): RestClientDouble {
            urlMappeResponses[url] = value
            return this@RestClientDouble
        }
    }

    fun forUrl(url: String): UrlMappeResponsesBuilder {
        return UrlMappeResponsesBuilder(url)
    }

    fun willReturn(v: Any?): RestClientDouble {
        responses.add(v)
        return this
    }

    override fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T? {
        if (urlMappeResponses.containsKey(url.toString())) {
            return urlMappeResponses[url.toString()] as T?
        }

        if (responses.isEmpty())
            return null

        return responses.removeFirst() as T?

    }

    override fun <T : Any> get(url: URL, payload: String, type: KClass<T>, debug: Boolean): T? {
        if (urlMappeResponses.containsKey(url.toString())) {
            return urlMappeResponses[url.toString()] as T?
        }

        if (responses.isEmpty())
            return null

        return responses.removeFirst() as T?
    }
}
