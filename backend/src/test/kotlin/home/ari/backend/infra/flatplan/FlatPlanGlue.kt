package home.ari.backend.infra.flatplan

import home.ari.backend.domain.flatplan.doubles.RoomPositionsDouble
import home.ari.backend.domain.flatplan.model.FlatPlan
import home.ari.backend.domain.flatplan.model.Position
import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.infra.TestContext
import home.ari.backend.infra.doubles.SonosDiscoveryDouble
import home.ari.backend.infra.doubles.SonosPlayersMaker
import home.ari.backend.infra.doubles.SonosRestClientDouble
import io.cucumber.datatable.DataTable
import io.cucumber.java.Before
import io.cucumber.java.PendingException
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


class FlatPlanGlue(
    val mockMvc: MockMvc,
    val context: TestContext,
    val roomPositionsDouble: RoomPositionsDouble,
    val sonosPlayersMaker: SonosPlayersMaker
) {

    var expectedFlatPlan = FlatPlan("", listOf())

    @Before
    fun reset() {
        expectedFlatPlan = FlatPlan("", listOf())

    }

    private fun create(name: String) {
        sonosPlayersMaker.createPlayer(name)
    }

    @Given("my {string} with:")
    fun my_with(name: String, dataTable: DataTable) {
        val rooms = dataTable.asLists().map { (name, points) -> Room(name, points.toPoints(), Position(0.0, 0.0)) }
        roomPositionsDouble.rooms.addAll(rooms)
    }

    @Given("I have players in:")
    fun i_have_players_in(rooms: DataTable) {
        rooms.asList().map { create(it) }
    }

    @When("I want to display my flat plan")
    fun i_want_to_display_my_flat_plan() {
        context.r = mockMvc.perform(MockMvcRequestBuilders.get("/flat-plan/home"))
    }

    @Then("I should see my rooms")
    fun i_should_see_my_rooms() {
        context.r.andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                    MockMvcResultMatchers.jsonPath("$.rooms[*].name").value(roomPositionsDouble.rooms.map { it.name })
            )
    }

    @Then("I should know how to retrieve the metadata in {string}")
    fun i_should_know_how_to_retrieve_the_metadata_in(room: String) {
        context.r.andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                    MockMvcResultMatchers.jsonPath("$.rooms[?(@.name == '$room')].playerInformationUrl").isNotEmpty
            )
    }
}

private fun String.toPoints(): List<Position> {
    return this.split(" ").map { val (x, y) = it.split(";"); Position(x.toDouble(), y.toDouble()) }
}
