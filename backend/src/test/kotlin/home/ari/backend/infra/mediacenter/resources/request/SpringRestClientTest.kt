package home.ari.backend.infra.mediacenter.resources.request

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.*
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.RestTemplate
import java.net.URL

/*
@ContextConfiguration(classes = [TestConfig::class])
@SpringBootTest
class SpringRestClientTest {
    @Autowired
    lateinit var client: SpringRestClient

    @Autowired
    lateinit var restTemplate: RestTemplate


    @Test
    fun `can get info from the sonos device`() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity<Class<String>>(headers)

        val url = URL("http://localhost:8080/")
        Mockito.`when`(restTemplate.exchange(url.toString(), HttpMethod.GET, request, String::class.java, "")).thenReturn(
                ResponseEntity.ok("<body>this is my response</body>"))
        assertThat(client.get<String>(url)).isEqualTo(
                "<body>this is my response</body>"
        )
    }
}

@ComponentScan(
        basePackages = ["home.ari.backend.infra.mediacenter.resources.request"]
)
@TestConfiguration
class TestConfig {

    val restTemplate: RestTemplate = Mockito.mock(RestTemplate::class.java)


    @Bean
    fun restTemplate(): RestTemplate {
        return restTemplate
    }

    @Bean
    fun restTemplateBuilder(): RestTemplateBuilder {
        val rtb: RestTemplateBuilder = Mockito.mock(RestTemplateBuilder::class.java)

        Mockito.`when`(rtb.build()).thenReturn(restTemplate)

        return rtb
    }

}
*/
