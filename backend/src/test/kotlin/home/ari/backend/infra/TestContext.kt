package home.ari.backend.infra

import io.cucumber.spring.ScenarioScope
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.ResultActions

@ScenarioScope
@Component
class TestContext {
    lateinit var r: ResultActions
}
