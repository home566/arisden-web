package home.ari.backend.infra.doubles

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.rpSong
import home.ari.backend.infra.mediacenter.resources.model.sonos.ZPSupportInfo
import org.mockito.Mockito
import org.mockito.stubbing.Answer
import org.springframework.http.*
import org.springframework.web.client.RestTemplate

class SonosPlayersMaker(
    val restTemplate: RestTemplate,
    val sonosDiscoveryDouble: SonosDiscoveryDouble
) {


    fun reset() {
        Mockito.reset(restTemplate)
    }


    fun createPlayer(name: String) {
        println("Player: $name, ${Factories.device(name)}")
        sonosDiscoveryDouble.create(name)
        createStatus(name)
        createState(name)
        createTrack(name, Song.nothing())
        createControlActions(name)
        enableRP()
    }

    private fun createControlActions(room: String) {
        fun create(action: String, stateAfter: String) {
            val device = Factories.device(room)
            val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
            val headers = HttpHeaders()
            headers.add("SOAPACTION", "urn:schemas-upnp-org:service:AVTransport:1#$action")
            headers.contentType = MediaType.APPLICATION_XML

            val payload = """
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:$action xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><Speed>1</Speed><InstanceID>0</InstanceID></u:$action></s:Body></s:Envelope>
        """.trimIndent()
            val request = HttpEntity(payload, headers)

            val u = Answer<HttpEntity<*>> {
                createState(room, stateAfter)
                ResponseEntity.ok("")
            }

            Mockito.`when`(restTemplate.exchange(url, HttpMethod.POST, request, String::class.java))
                .thenAnswer(u)
        }

        create("Play", "PLAYING")
        create("Stop", "STOPPED")
    }


    private fun createState(room: String, state: String = "PAUSED_PLAYBACK") {
        println("create state $room")
        val device = Factories.device(room)
        val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
        val headers = HttpHeaders()
        headers.add("SOAPACTION", "urn:schemas-upnp-org:service:AVTransport:1#GetTransportInfo")
        headers.contentType = MediaType.APPLICATION_XML

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetTransportInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetTransportInfo></s:Body></s:Envelope>
        """.trimIndent()
        val request = HttpEntity(payload, headers)

        Mockito.`when`(restTemplate.exchange(url, HttpMethod.POST, request, String::class.java))
            .thenReturn(ResponseEntity.ok("""
                <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <s:Body>
                        <u:GetTransportInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                            <CurrentTransportState>$state</CurrentTransportState>
                            <CurrentTransportStatus>OK</CurrentTransportStatus>
                            <CurrentSpeed>1</CurrentSpeed>
                        </u:GetTransportInfoResponse>
                    </s:Body>
                </s:Envelope>
            """.trimIndent()))
    }

    private fun command(url: String, soapAction: String, payload: String, response: String) {
        val headers = HttpHeaders()
        headers.add("SOAPACTION", soapAction)
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity(payload, headers)

        Mockito.`when`(restTemplate.exchange(url, HttpMethod.POST, request, String::class.java))
            .thenReturn(ResponseEntity.ok(response))
    }

    private fun createTrack(room: String, song: Song) {
        println("create track $room")
        val device = Factories.device(room)
        val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
        val soapAction = "urn:schemas-upnp-org:service:AVTransport:1#GetPositionInfo"

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetPositionInfo></s:Body></s:Envelope>
        """.trimIndent()

        val response = """
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <s:Body>
                    <u:GetPositionInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                        <Track>1</Track>
                        <TrackDuration>0:04:24</TrackDuration>
                        <TrackMetaData>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns:r=&quot;urn:schemas-rinconnetworks-com:metadata-1-0/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot;&gt;&lt;item id=&quot;-1&quot; parentID=&quot;-1&quot; restricted=&quot;true&quot;&gt;&lt;res protocolInfo=&quot;sonos.com-spotify:*:audio/x-spotify:*&quot; duration=&quot;0:04:24&quot;&gt;x-sonos-spotify:spotify%3atrack%3a3d9DChrdc6BOeFsbrZ3Is0?sid=9&amp;amp;flags=8224&amp;amp;sn=1&lt;/res&gt;&lt;r:streamContent&gt;&lt;/r:streamContent&gt;&lt;upnp:albumArtURI&gt;/getaa?s=1&amp;amp;u=x-sonos-spotify%3aspotify%253atrack%253a3d9DChrdc6BOeFsbrZ3Is0%3fsid%3d9%26flags%3d8224%26sn%3d1&lt;/upnp:albumArtURI&gt;&lt;dc:title&gt;${song.title}&lt;/dc:title&gt;&lt;upnp:class&gt;object.item.audioItem.musicTrack&lt;/upnp:class&gt;&lt;dc:creator&gt;${song.artist}&lt;/dc:creator&gt;&lt;upnp:album&gt;${song.album}&lt;/upnp:album&gt;&lt;/item&gt;&lt;/DIDL-Lite&gt;</TrackMetaData>
                        <TrackURI>x-sonos-spotify:spotify%3atrack%3a3d9DChrdc6BOeFsbrZ3Is0?sid=9&amp;flags=8224&amp;sn=1</TrackURI>
                        <RelTime>0:00:03</RelTime>
                        <AbsTime>NOT_IMPLEMENTED</AbsTime>
                        <RelCount>2147483647</RelCount>
                        <AbsCount>2147483647</AbsCount>
                    </u:GetPositionInfoResponse>
                </s:Body>
            </s:Envelope>
        """.trimIndent()

        command(url, soapAction, payload, response)

    }

    private fun createStatus(room: String) {
        println("create status $room")
        val device = Factories.device(room)
        val infoUrl = "http://${device.address}:1400/status/zp"
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity<Class<String>>(headers)

        val responseObject = ZPSupportInfo()
        responseObject.info.name = room
        Mockito.`when`(restTemplate.exchange(infoUrl, HttpMethod.GET, request, ZPSupportInfo::class.java, ""))
            .thenReturn(ResponseEntity.ok(responseObject))
    }

    fun positionInfo(room: String, response: String) {
        println("position info $room")
        val device = Factories.device(room)
        val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
        val soapAction = "urn:schemas-upnp-org:service:AVTransport:1#GetPositionInfo"

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetPositionInfo></s:Body></s:Envelope>
        """.trimIndent()
        command(url, soapAction, payload, response)
    }

    fun startRPIn(room: String) {
        val device = Factories.device(room)
        val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
        val soapAction = "urn:schemas-upnp-org:service:AVTransport:1#GetPositionInfo"

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetPositionInfo></s:Body></s:Envelope>
        """.trimIndent()

        val response = """
                    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                        <s:Body>
                            <u:GetPositionInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                                <Track>1</Track>
                                <TrackDuration>0:00:00</TrackDuration>
                                <TrackMetaData>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns:r=&quot;urn:schemas-rinconnetworks-com:metadata-1-0/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot;&gt;&lt;item id=&quot;-1&quot; parentID=&quot;-1&quot; restricted=&quot;true&quot;&gt;&lt;res protocolInfo=&quot;aac:*:application/octet-stream:*&quot;&gt;aac://http://stream-tx3.radioparadise.com/aac-320&lt;/res&gt;&lt;r:streamContent&gt;Bob Marley - Sun Is Shining&lt;/r:streamContent&gt;&lt;dc:title&gt;aac-320&lt;/dc:title&gt;&lt;upnp:class&gt;object.item&lt;/upnp:class&gt;&lt;/item&gt;&lt;/DIDL-Lite&gt;</TrackMetaData>
                                <TrackURI>aac://http://stream-tx3.radioparadise.com/aac-320</TrackURI>
                                <RelTime>0:03:26</RelTime>
                                <AbsTime>NOT_IMPLEMENTED</AbsTime>
                                <RelCount>2147483647</RelCount>
                                <AbsCount>2147483647</AbsCount>
                            </u:GetPositionInfoResponse>
                        </s:Body>
                    </s:Envelope>
                """.trimIndent()
        command(url, soapAction, payload, response)
    }

    fun startSpotifyIn(room: String, song: Song) {
        positionInfo(room,
        """
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                <s:Body>
                    <u:GetPositionInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                        <Track>1</Track>
                        <TrackDuration>0:04:24</TrackDuration>
                        <TrackMetaData>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns:r=&quot;urn:schemas-rinconnetworks-com:metadata-1-0/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot;&gt;&lt;item id=&quot;-1&quot; parentID=&quot;-1&quot; restricted=&quot;true&quot;&gt;&lt;res protocolInfo=&quot;sonos.com-spotify:*:audio/x-spotify:*&quot; duration=&quot;0:04:24&quot;&gt;x-sonos-spotify:spotify%3atrack%3a3d9DChrdc6BOeFsbrZ3Is0?sid=9&amp;amp;flags=8224&amp;amp;sn=1&lt;/res&gt;&lt;r:streamContent&gt;&lt;/r:streamContent&gt;&lt;upnp:albumArtURI&gt;/getaa?s=1&amp;amp;u=x-sonos-spotify%3aspotify%253atrack%253a3d9DChrdc6BOeFsbrZ3Is0%3fsid%3d9%26flags%3d8224%26sn%3d1&lt;/upnp:albumArtURI&gt;&lt;dc:title&gt;${song.title}&lt;/dc:title&gt;&lt;upnp:class&gt;object.item.audioItem.musicTrack&lt;/upnp:class&gt;&lt;dc:creator&gt;${song.artist}&lt;/dc:creator&gt;&lt;upnp:album&gt;${song.album}&lt;/upnp:album&gt;&lt;/item&gt;&lt;/DIDL-Lite&gt;</TrackMetaData>
                        <TrackURI>x-sonos-spotify:spotify%3atrack%3a3d9DChrdc6BOeFsbrZ3Is0?sid=9&amp;flags=8224&amp;sn=1</TrackURI>
                        <RelTime>0:00:03</RelTime>
                        <AbsTime>NOT_IMPLEMENTED</AbsTime>
                        <RelCount>2147483647</RelCount>
                        <AbsCount>2147483647</AbsCount>
                    </u:GetPositionInfoResponse>
                </s:Body>
            </s:Envelope>
        """.trimIndent())
    }

    fun createUnknownDevice() {
        sonosDiscoveryDouble.create("unknown")
        createPlayer("unknown")
        disablePlayer("unknown")
    }

    fun enableRP() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity<Class<String>>(headers)
        Mockito.`when`(restTemplate.exchange("https://api.radioparadise.com/api/get_block?info=true", HttpMethod.GET, request, String::class.java, ""))
            .thenReturn(ResponseEntity.ok("""{
    "event": "2245830",
    "sched_time_millis": 1643402343089,
    "type": "M",
    "end_event": "2245832",
    "length": "754.7",
    "url": "https://audio-geo.radioparadise.com/blocks/chan/0/1/2245830-2245832.m4a",
    "timeout_millis": 900000,
    "chan": "0",
    "channel": {
        "chan": "0",
        "title": "RP Main Mix",
        "stream_name": "main"
    },
    "bitrate": "64k aac",
    "ext": "m4a",
    "cue": 332000,
    "expiration": 1643489001,
    "filename": {
        "0": "2245830-2245832"
    },
    "image_base": "//img.radioparadise.com/",
    "song": {
        "0": {
            "event": "2245830",
            "type": "M",
            "sched_time_millis": 1643402343089,
            "song_id": "40248",
            "duration": 338000,
            "artist": "${rpSong.artist}",
            "title": "${rpSong.title}",
            "album": "${rpSong.album}",
            "year": "1998",
            "asin": "B000007QD7",
            "rating": "6.78",
            "slideshow": "37801,36066,68807,23030,40084,41538,45341,36754,22297,41910,45879,29538,34574,21733,40789,23336,29905,43956,43238,49264,37214,21460,40569,34681,36008,7901,40895,67746,47156,66531,57518,35633,58940,45740,53825",
            "user_rating": null,
            "cover": "covers/l/B000007QD7.jpg",
            "elapsed": 0,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2245830.m4a"
        },
        "1": {
            "event": "2245831",
            "type": "M",
            "sched_time_millis": 1643402681089,
            "song_id": "32413",
            "duration": 223700,
            "artist": "Tanya Donelly",
            "title": "Moonbeam Monkey",
            "album": "Beautysleep",
            "year": "2002",
            "asin": "B00005UCZP",
            "rating": "6.79",
            "slideshow": "38181,3369,3370,3379,44407,46536,3027,37993,3385,46947,3374,36663,2469,54727,8168,3377,29664,3372,68206,3380,33381,8165,3371,21265",
            "user_rating": null,
            "cover": "covers/l/B00005UCZP.jpg",
            "elapsed": 338000,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2245831.m4a"
        },
        "2": {
            "event": "2245832",
            "type": "M",
            "sched_time_millis": 1643402904789,
            "song_id": "38754",
            "duration": 193000,
            "artist": "Pink Martini",
            "title": "Hang On Little Tomato",
            "album": "Hang on Little Tomato",
            "year": "2004",
            "asin": "B0002S94WK",
            "rating": "7.32",
            "slideshow": "40587,19398,19396,19400,61524,19399,19395,19401,5626,10453,57270,19391,19394,8393,19393,19397,51036,8388,19392,45006,13344",
            "user_rating": null,
            "cover": "covers/l/B0002S94WK.jpg",
            "elapsed": 561700,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2245832.m4a"
        }
    },
    "cache_ahead_timeout": {
        "0": {
            "cache_length_millis_max": 1800000,
            "timeout_millis": 900000
        },
        "1": {
            "cache_length_millis_max": 3600000,
            "timeout_millis": 1800000
        },
        "2": {
            "cache_length_millis_max": 5400000,
            "timeout_millis": 2700000
        },
        "3": {
            "cache_length_millis_max": 7200000,
            "timeout_millis": 3600000
        },
        "4": {
            "cache_length_millis_max": 10800000,
            "timeout_millis": 5400000
        },
        "5": {
            "cache_length_millis_max": 14400000,
            "timeout_millis": 7200000
        },
        "6": {
            "cache_length_millis_max": 18000000,
            "timeout_millis": 9000000
        },
        "7": {
            "cache_length_millis_max": 21600000,
            "timeout_millis": 10800000
        },
        "8": {
            "cache_length_millis_max": 9223372036854775807,
            "timeout_millis": 14400000
        }
    },
    "current_event_id": 2245830,
    "max_gapless_event_id": 2245860
}""".trimEnd()))
    }

    fun disableRP() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val request = HttpEntity<Class<String>>(headers)
        Mockito.`when`(restTemplate.exchange("https://api.radioparadise.com/api/get_block?info=true", HttpMethod.GET, request, String::class.java, ""))
            .thenReturn(ResponseEntity.ok("{}"))
    }

    fun busyPlayer(room: String) {
        val device = Factories.device(room)
        val url = "http://${device.address}:1400/MediaRenderer/AVTransport/Control"
        val soapAction = "urn:schemas-upnp-org:service:AVTransport:1#GetPositionInfo"

        val payload ="""
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body><u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1"><InstanceID>0</InstanceID><Channel>Master</Channel></u:GetPositionInfo></s:Body></s:Envelope>
        """.trimIndent()
        var headers = HttpHeaders()
        headers.add("SOAPACTION", soapAction)
        headers.contentType = MediaType.APPLICATION_XML
        var request = HttpEntity(payload, headers)

        Mockito.`when`(restTemplate.exchange(url, HttpMethod.POST, request, String::class.java))
            .thenReturn(ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(null))
    }

    fun disablePlayer(room: String) {
        val device = Factories.device(room)
        val infoUrl = "http://${device.address}:1400/status/zp"
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_XML
        val responseObject = ZPSupportInfo()
        responseObject.info.name = room
        Mockito.`when`(restTemplate.exchange(infoUrl, HttpMethod.GET, HttpEntity<Class<String>>(headers), ZPSupportInfo::class.java, ""))
            .thenReturn(ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(null))
    }
}


/*
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:GetTransportInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
            <CurrentTransportState>PAUSED_PLAYBACK</CurrentTransportState>
            <CurrentTransportStatus>OK</CurrentTransportStatus>
            <CurrentSpeed>1</CurrentSpeed>
        </u:GetTransportInfoResponse>
    </s:Body>
</s:Envelope>
 */
