package home.ari.backend.infra.flatplan.builder


import home.ari.backend.domain.flatplan.model.Position
import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.infra.flatplan.resources.plans.staticplans.PlanConfiguration
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test


class PlanTestHelper: PlanConfiguration() {


    fun invoke(init: PlanTestHelper.() -> Unit): PlanTestHelper {
        this.init()
        return this
    }

    fun `should build`(vararg room: Room) {
        val result = this.rooms
        Assertions.assertThat(result)
            .containsExactlyInAnyOrderElementsOf(room.toList())
    }
}

class RoomBuilderTest {

    @Test
    fun `should build a room`() {
        PlanTestHelper().invoke {
            this.room {
                name = "office"
                p(0, 0)
                p(10, 0)
                p(10, 10)
                p(0, 10)
                `player at`(5, 5)
            }
            this.room {
                name = "bathroom"
                p(20, 0)
                p(20, 0)
                p(20, 20)
                p(0, 20)
            }
        }.`should build` (
                Room(
                        "office",
                        listOf(Position(0.0, 10.0), Position(10.0, 0.0), Position(10.0, 10.0), Position(0.0, 10.0)),
                        Position(5.0, 5.0)
                ),
                Room(
                        "bathroom",
                        listOf(Position(0.0, 10.0), Position(10.0, 0.0), Position(10.0, 10.0), Position(0.0, 10.0)),
                        Position.NULL
                )
            )
    }
}
