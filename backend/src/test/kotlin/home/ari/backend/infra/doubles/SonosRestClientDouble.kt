package home.ari.backend.infra.doubles

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.Device
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.rpSong
import home.ari.backend.domain.mediacenter.spotifySong
import home.ari.backend.infra.mediacenter.resources.model.sonos.DeviceInfo
import home.ari.backend.infra.mediacenter.resources.model.sonos.ZPSupportInfo
import home.ari.backend.infra.mediacenter.resources.request.RestClient
import org.springframework.http.HttpEntity
import java.net.URL
import kotlin.reflect.KClass

class SonosRestClientDouble: RestClient {

    val devices = mutableMapOf<String, String>()
    val songPlaying = mutableMapOf<String, Song>()

    val playerPlaying = mutableListOf<String>()

    val radioIn = mutableListOf<String>()


    var rpDoesNotRespond = false

    val devicesCrashed = mutableListOf<String>()

    fun create(device: String, room: String=device): Device {
        devices[device] = room
        return Factories.device(device)
    }

    fun songIn(name: String, song: Song) {
        songPlaying[name] = song
        playerPlaying.add(name)
    }

    fun unplug(device: String) {
        devices.remove(device)
    }

    override fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T? {
        val (device, room) = devices.entries.find { (device, _) -> url.toString().contains(Factories.device(device).address) } ?: return null
        if (devicesCrashed.contains(device))
            return null

        if ((request.body?: "").contains("GetTransportInfo"))
            return playingStateFor(device) as T

        if ((request.body?: "").contains("Play")) {
            this.songPlaying[device] = spotifySong
            return playingStateFor(device) as T
        } else if ((request.body?: "").contains("Stop")) {
            //songPlaying.remove(device)
            //radioIn.remove(device)
            playerPlaying.remove(device)
            return playingStateFor(device) as T
        }


        if (radioIn.contains(device))
            return currentTrackRP() as T
        return currentTrack(songPlaying.getOrDefault(device, Song.nothing())) as T
    }

    override fun <T : Any> get(url: URL, payload: String, type: KClass<T>, debug: Boolean): T? {
        if (url == URL("https://api.radioparadise.com/api/get_block?info=true")) {
            if (rpDoesNotRespond)
                return "{}" as T
            return radioParadise(rpSong) as T
        }

        val (device, room) = devices.entries.find { (device, _) -> url.toString().contains(Factories.device(device).address) } ?: return null
        return info(device, room) as T
    }

    fun reset() {
        devices.clear()
        devicesCrashed.clear()
        rpDoesNotRespond = false
    }

    fun radioIn(room: String): Song {
        radioIn.add(room)
        playerPlaying.add(room)
        return rpSong
    }

    fun playingStateFor(room: String): String {
        val state = if (this.playerPlaying.contains(room)) {
            "PLAYING"
        } else {
            "STOPPED"
        }
        return """<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:GetTransportInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
            <CurrentTransportState>${state}</CurrentTransportState>
            <CurrentTransportStatus>OK</CurrentTransportStatus>
            <CurrentSpeed>1</CurrentSpeed>
        </u:GetTransportInfoResponse>
    </s:Body>
</s:Envelope>"""}

    fun crash(device: String) {
        devicesCrashed.add(device)
    }

    fun disableRP() {
        rpDoesNotRespond = true
    }
}

fun info(device: String, room: String): ZPSupportInfo {
    val info = ZPSupportInfo()
    info.info = DeviceInfo()
    info.info.name = room

    return info
}


fun currentTrack(song: Song) ="""
    <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
        <s:Body>
            <u:GetPositionInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                <Track>6</Track>
                <TrackDuration>0:06:31</TrackDuration>
                <TrackMetaData>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns:r=&quot;urn:schemas-rinconnetworks-com:metadata-1-0/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot;&gt;&lt;item id=&quot;-1&quot; parentID=&quot;-1&quot; restricted=&quot;true&quot;&gt;&lt;res protocolInfo=&quot;sonos.com-spotify:*:audio/x-spotify:*&quot; duration=&quot;0:06:31&quot;&gt;x-sonos-spotify:spotify%3atrack%3a7uB6JnZJd0zg4q8lp4Ni89?sid=9&amp;amp;flags=8224&amp;amp;sn=1&lt;/res&gt;&lt;r:streamContent&gt;&lt;/r:streamContent&gt;&lt;upnp:albumArtURI&gt;/getaa?s=1&amp;amp;u=x-sonos-spotify%3aspotify%253atrack%253a7uB6JnZJd0zg4q8lp4Ni89%3fsid%3d9%26flags%3d8224%26sn%3d1&lt;/upnp:albumArtURI&gt;&lt;dc:title&gt;${song.title}&lt;/dc:title&gt;&lt;upnp:class&gt;object.item.audioItem.musicTrack&lt;/upnp:class&gt;&lt;dc:creator&gt;${song.artist}&lt;/dc:creator&gt;&lt;upnp:album&gt;${song.album}&lt;/upnp:album&gt;&lt;/item&gt;&lt;/DIDL-Lite&gt;</TrackMetaData>
                <TrackURI>x-sonos-spotify:spotify%3atrack%3a7uB6JnZJd0zg4q8lp4Ni89?sid=9&amp;flags=8224&amp;sn=1</TrackURI>
                <RelTime>0:05:16</RelTime>
                <AbsTime>NOT_IMPLEMENTED</AbsTime>
                <RelCount>2147483647</RelCount>
                <AbsCount>2147483647</AbsCount>
            </u:GetPositionInfoResponse>
        </s:Body>
    </s:Envelope>
""".trimIndent()


fun currentTrackRP() = """<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:GetPositionInfoResponse xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
            <Track>1</Track>
            <TrackDuration>0:00:00</TrackDuration>
            <TrackMetaData>&lt;DIDL-Lite xmlns:dc=&quot;http://purl.org/dc/elements/1.1/&quot; xmlns:upnp=&quot;urn:schemas-upnp-org:metadata-1-0/upnp/&quot; xmlns:r=&quot;urn:schemas-rinconnetworks-com:metadata-1-0/&quot; xmlns=&quot;urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/&quot;&gt;&lt;item id=&quot;-1&quot; parentID=&quot;-1&quot; restricted=&quot;true&quot;&gt;&lt;res protocolInfo=&quot;aac:*:application/octet-stream:*&quot;&gt;aac://http://stream-tx3.radioparadise.com/aac-320&lt;/res&gt;&lt;r:streamContent&gt;The Clash - Clampdown&lt;/r:streamContent&gt;&lt;dc:title&gt;aac-320&lt;/dc:title&gt;&lt;upnp:class&gt;object.item&lt;/upnp:class&gt;&lt;/item&gt;&lt;/DIDL-Lite&gt;</TrackMetaData>
            <TrackURI>aac://http://stream-tx3.radioparadise.com/aac-320</TrackURI>
            <RelTime>0:00:02</RelTime>
            <AbsTime>NOT_IMPLEMENTED</AbsTime>
            <RelCount>2147483647</RelCount>
            <AbsCount>2147483647</AbsCount>
        </u:GetPositionInfoResponse>
    </s:Body>
</s:Envelope>""".trimEnd()


fun radioParadise(song: Song) = """{
    "event": "2226554",
    "sched_time_millis": 1639255316026,
    "type": "M",
    "end_event": "2226557",
    "length": "891.9",
    "url": "https://audio-geo.radioparadise.com/blocks/chan/0/1/2226554-2226557.m4a",
    "timeout_millis": 900000,
    "chan": "0",
    "channel": {
        "chan": "0",
        "title": "RP Main Mix",
        "stream_name": "main"
    },
    "bitrate": "64k aac",
    "ext": "m4a",
    "cue": 23000,
    "expiration": 1639341801,
    "filename": {
        "0": "2226554-2226557"
    },
    "image_base": "//img.radioparadise.com/",
    "song": {
        "0": {
            "event": "2226554",
            "type": "M",
            "sched_time_millis": 1639255316026,
            "song_id": "41968",
            "duration": 235500,
            "artist": "${song.artist}",
            "title": "${song.title}",
            "album": "${song.album}",
            "year": "2009",
            "asin": "B0025AY45A",
            "rating": "7.13",
            "slideshow": "53977,47547,45909,53968,51147,23858,54059,15482,53665,55891,51194,39745,59460,28772,55247,67659,40520,70191,67376,38482,57866,57663,42513,148,59435",
            "user_rating": null,
            "cover": "covers/l/B0025AY45A.jpg",
            "elapsed": 0,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2226554.m4a"
        },
        "1": {
            "event": "2226555",
            "type": "M",
            "sched_time_millis": 1639255551526,
            "song_id": "20002",
            "duration": 258400,
            "artist": "Rusted Root",
            "title": "Send Me On My Way",
            "album": "When I Woke",
            "year": "1994",
            "asin": "B000001E5Z",
            "rating": "6.88",
            "slideshow": "63146,45210,54987,33498,18078,67522,51959,36433,61375,27561,69795,58299,42389,61053,29741,29222,61610,54425,38264,55714,35676,46833,56368,67675,31132,61709,42365",
            "user_rating": null,
            "cover": "covers/l/B000001E5Z.jpg",
            "elapsed": 235500,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2226555.m4a"
        },
        "2": {
            "event": "2226556",
            "type": "M",
            "sched_time_millis": 1639255809926,
            "song_id": "44236",
            "duration": 209000,
            "artist": "The Shins",
            "title": "Turn On Me",
            "album": "Wincing the Night Away",
            "year": "2007",
            "asin": "B000K2VHN2",
            "rating": "7.09",
            "slideshow": "35579,54136,48075,55508,20942,34989,66051,12669,46373,64782,17072,61223,42644,65575,24160,58684,58393,50431,28077,59585,35358,56490",
            "user_rating": null,
            "cover": "covers/l/B000K2VHN2.jpg",
            "elapsed": 493900,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2226556.m4a"
        },
        "3": {
            "event": "2226557",
            "type": "M",
            "sched_time_millis": 1639256018926,
            "song_id": "1671",
            "duration": 189000,
            "artist": "John Mellencamp",
            "title": "The Full Catastrophe",
            "album": "Mr. Happy Go Lucky",
            "year": "1995",
            "asin": "B000A0EMI6",
            "rating": "7.29",
            "slideshow": "63206,16172,29445,59587,12447,13071,26655,66333,9016,57779,30271,65411,33716,55287,42140,67918,45546,51049,26367,66317",
            "user_rating": null,
            "cover": "covers/l/B000A0EMI6.jpg",
            "elapsed": 702900,
            "timeout_millis": 900000,
            "gapless_url": "https://audio-geo.radioparadise.com/audio/gapless/0/1/2226557.m4a"
        }
    },
    "cache_ahead_timeout": {
        "0": {
            "cache_length_millis_max": 1800000,
            "timeout_millis": 900000
        },
        "1": {
            "cache_length_millis_max": 3600000,
            "timeout_millis": 1800000
        },
        "2": {
            "cache_length_millis_max": 5400000,
            "timeout_millis": 2700000
        },
        "3": {
            "cache_length_millis_max": 7200000,
            "timeout_millis": 3600000
        },
        "4": {
            "cache_length_millis_max": 10800000,
            "timeout_millis": 5400000
        },
        "5": {
            "cache_length_millis_max": 14400000,
            "timeout_millis": 7200000
        },
        "6": {
            "cache_length_millis_max": 18000000,
            "timeout_millis": 9000000
        },
        "7": {
            "cache_length_millis_max": 21600000,
            "timeout_millis": 10800000
        },
        "8": {
            "cache_length_millis_max": 9223372036854775807,
            "timeout_millis": 14400000
        }
    },
    "current_event_id": 2226554,
    "max_gapless_event_id": 2226584
}"""
