package home.ari.backend.infra.mediacenter.resources

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.rpSong
import home.ari.backend.domain.mediacenter.spotifySong
import home.ari.backend.domain.mediacenter.withState
import home.ari.backend.infra.doubles.SonosDiscoveryDouble
import home.ari.backend.infra.doubles.SonosRestClientDouble
import home.ari.backend.infra.mediacenter.resources.sonos.SonosAPI
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class SonosAPITest {

    @Test
    fun `should return the room of the given device`() {
        `with players`{
            "player 1" `in` "bedroom"
            "player 2" `in` "living room"
        }.`do` {
          `the room of`("player 1") `should be` "bedroom"
        }
    }

    @Test
    fun `should return all players`() {
        `with players`{
            "player 1" `in` "bedroom"
            "player 2" `in` "living room"
        }.`do` {
            all().`should be`(Factories.player(room="bedroom", device="player 1"),
                              Factories.player(room="living room", device="player 2"))
        }
    }

    @Test
    fun `should return the song playing in a room`() {
        `with players`{
            "player 1" `in` "bedroom"  `where I play` rpSong
            "player 2" `in` "living room"  `where I play` spotifySong
        }.`do` {
            all().`should be`(
                    Factories.player(room="bedroom", device="player 1", song=rpSong).withState(PlayingState.Playing),
                    Factories.player(room="living room", device="player 2", song=spotifySong).withState(PlayingState.Playing)
            )
        }
    }

    @Test
    fun `should handle RP is not responding`() {
        `with players`{
            ("player 1" `in` "bedroom").`but RP is down`()
        }.`do` {
            all().`should be`(
                    Factories.player(room="bedroom", device="player 1", song=rpError("player 1")).withState(PlayingState.Playing),
            )
        }
    }

    @Test
    fun `should not list not responding player`() {
        `with players`{
            "player 1" `in` "bedroom"
            - "player 2"
        }.`do` {
            all().`should be`(
                    Factories.player(room="bedroom", device="player 1"),
            )
        }
    }

    @Test
    fun `should handle player is not responding`() {
        `with players`{
            ("player 1" `in` "bedroom").`but player is down`()
        }.`do` {
            all().`should be`(
                    Factories.player(room="bedroom", device="player 1").withState(
                            Either.fail(PlayerIsNotResponding(Factories.device("player 1")))
                    ),
            )
        }
    }

    @Test
    fun `should configure a room`() {
        `with players`{
            "player 1" `in` "bedroom"  `where I play` rpSong
            "player 2" `in` "living room"  `where I play` spotifySong
        }.`do` {
            "player 1".stop()
            "player 2".stop()
            "player 1".play()
            all().`should be`(
                    Factories.player(room="bedroom", device="player 1", song= rpSong).withState(PlayingState.Playing),
                    Factories.player(room="living room", device="player 2", song=spotifySong).withState(PlayingState.Stopped)
            )
        }
    }


    class Helper {
        val client = SonosRestClientDouble()
        val sonosDiscoveryDouble = SonosDiscoveryDouble()

        val sonosAPI = SonosAPI(client, sonosDiscoveryDouble)

        infix fun String.`in`(room: String): StateBuilder {
            sonosDiscoveryDouble.create(this)
            client.create(this, room)
            return StateBuilder(this)
        }

        operator fun String.unaryMinus() {
            sonosDiscoveryDouble.create(this)
        }

        inner class StateBuilder(val device: String) {
            infix fun `where I play`(song: Song) {
                if (song == rpSong)
                    client.radioIn(device)
                client.songIn(device, song)
            }

            fun `but RP is down`() {
                client.radioIn(device)
                client.disableRP()
            }

            fun `but player is down`() {
                client.crash(device)
            }
        }

        fun String.stop() =sonosAPI.configure(Instructions() + Instruction(Factories.device(this), Action.Stop))
        fun String.play() =sonosAPI.configure(Instructions() + Instruction(Factories.device(this), Action.Play))

        fun `do`(test: Helper.() -> Unit) {
            test()
        }

        fun `the room of`(device: String): Either<RoomAccessError, Room> {
            return sonosAPI.getRoomOf(Factories.device(device))
        }

        fun all(): List<Player> {
            return sonosAPI.getAll()
        }

        infix fun Either<RoomAccessError, Room>.`should be`(expectedName: String) {
            this.fold(
                    { Assertions.fail("Expecting a value. Bu got the error $it") },
                    { assertThat(it).isEqualTo(Factories.room(expectedName)) }
            )
        }

        fun List<Player>.`should be`(vararg expected: Player) {
            data class allFields(val room: Room,
                                 val device: Device,
                                 val song: Either<PlayerError, Song>,
                                 val state: Either<PlayerError, PlayingState>)
            assertThat(this.map { allFields(it.room, it.device, it.song, it.state) })
                .containsExactlyInAnyOrder(*expected.map { allFields(it.room, it.device, it.song, it.state) }.toTypedArray())
        }

    }

    fun `with players`(init: Helper.() -> Unit): Helper {
        val helper = Helper()
        helper.init()

        return helper
    }
}




fun rpError(device: String): Either<PlayerError, Song> {
    return Either.fail(GetSongError(Factories.device(device), "Cannot retrieve Radio Paradise metadata"))
}
