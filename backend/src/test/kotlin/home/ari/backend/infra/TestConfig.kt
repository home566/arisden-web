package home.ari.backend.infra

import home.ari.backend.domain.annotations.DomainService
import home.ari.backend.domain.flatplan.doubles.RoomPositionsDouble
import home.ari.backend.infra.doubles.SonosDiscoveryDouble
import home.ari.backend.infra.doubles.SonosPlayersMaker
import home.ari.backend.infra.flatplan.rest.controllers.FlatPlanController
import home.ari.backend.infra.mediacenter.resources.sonos.discovery.SonosDiscovery
import home.ari.backend.infra.mediacenter.rest.controllers.MediaCenterController
import io.cucumber.java.Before
import io.cucumber.spring.CucumberContextConfiguration
import org.junit.jupiter.api.BeforeEach
import org.mockito.Mockito
import org.mockito.Mockito.withSettings
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.context.annotation.Primary
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.client.RestTemplate


@CucumberContextConfiguration
@WebMvcTest(controllers = [MediaCenterController::class, FlatPlanController::class])
@ContextConfiguration(classes = [TestConfig::class])
class CucumberSpringConfiguration { }


class Foobar(
    private val roomPositions: RoomPositionsDouble,
    private val sonosDiscoveryDouble: SonosDiscoveryDouble,
    private val sonosPlayersMaker: SonosPlayersMaker
) {
    @Before
    fun reset() {
        println("RESET")
        sonosDiscoveryDouble.reset()
        roomPositions.rooms.clear()
        sonosPlayersMaker.reset()
    }
}

@TestConfiguration
@ComponentScan(
        basePackages = ["home.ari.backend.domain", "home.ari.backend.infra"],
        includeFilters = [ComponentScan.Filter(type = FilterType.ANNOTATION, value = [DomainService::class])]
)
class TestConfig {
    val restTemplate: RestTemplate = Mockito.mock(RestTemplate::class.java, withSettings().verboseLogging())

    private val roomPositions = RoomPositionsDouble()
    private val sonosDiscoveryDouble = SonosDiscoveryDouble()
    private val sonosPlayersMaker = SonosPlayersMaker(restTemplate, sonosDiscoveryDouble)

    @Bean
    @Primary
    fun getSocket(): SonosDiscovery {
        return sonosDiscoveryDouble
    }

    @Bean
    @Primary
    fun getRoomPositionsDouble(): RoomPositionsDouble {
        return roomPositions
    }

    @Bean
    fun getSonosPlayersMaker(): SonosPlayersMaker {
        return sonosPlayersMaker
    }

    @Bean
    fun restTemplateBuilder(): RestTemplateBuilder {
        val rtb: RestTemplateBuilder = Mockito.mock(RestTemplateBuilder::class.java)

        Mockito.`when`(rtb.build()).thenReturn(restTemplate)

        return rtb
    }

    /*@Bean
    fun getContext(): TestContext {
        return TestContext()
    }*/
}
