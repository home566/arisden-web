package home.ari.backend.infra.mediacenter.resources

import home.ari.backend.domain.mediacenter.model.Device
import home.ari.backend.infra.mediacenter.resources.sonos.devicesaccess.CommandClient
import home.ari.backend.infra.mediacenter.resources.request.RestClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

import org.springframework.http.HttpEntity
import java.net.URL
import kotlin.reflect.KClass


class CommandClientTest {

    @Test
    @Tag("technical")
    fun `should build a command correctly`() {
        val check = PostCommandChecked()
        val sut = CommandClient(check)

        sut.send(Device("localhost"), "GetPositionInfo", "AVTransport", false) { "" }

        check `that url is` "http://localhost:1400/MediaRenderer/AVTransport/Control"
        check `that payload is` """
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">            
                <s:Body>
                    <u:GetPositionInfo xmlns:u="urn:schemas-upnp-org:service:AVTransport:1">
                        <InstanceID>0</InstanceID>
                        <Channel>Master</Channel>
                    </u:GetPositionInfo>
                </s:Body>
            </s:Envelope>
        """
        check `that soap action is` "urn:schemas-upnp-org:service:AVTransport:1#GetPositionInfo"
    }
}


class PostCommandChecked: RestClient {
    lateinit var postedUrl: URL
    lateinit var postedRequest: HttpEntity<String>

    override fun <T : Any> post(url: URL, request: HttpEntity<String>, type: KClass<T>, debug: Boolean): T? {
        postedUrl = url
        postedRequest = request
        return null
    }

    override fun <T : Any> get(url: URL, payload: String, type: KClass<T>, debug: Boolean): T? {
        TODO("Not yet implemented")
    }

    infix fun `that url is`(expected: String) {
        Assertions.assertEquals(URL(expected), postedUrl)
    }

    infix fun `that payload is`(expected: String) {
        val minifiedExpected = expected.split("\n").filter { it.isNotBlank() }.joinToString("") { it.trim() }
        Assertions.assertEquals(minifiedExpected, postedRequest.body)
    }

    infix fun `that soap action is`(expected: String) {
        Assertions.assertEquals(listOf(expected), postedRequest.headers["SOAPACTION"])
    }
}
