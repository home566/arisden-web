package home.ari.backend.infra.mediacenter.resources

import home.ari.backend.domain.mediacenter.model.PlayerError
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.infra.doubles.RestClientDouble
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class NowPlayingOnRPTest {

    @Test
    fun `should get current song on RP`() {
        val double = RestClientDouble()
            .willReturn(validRP)

        val result = NowPlayingOnRadioParadise(double)()
        Assertions.assertThat(result).isEqualTo(Either.success<PlayerError, Song>(Song(artist = "AC/DC", title= "Highway to Hell", album = "Best Of AC/DC", source= Source.RadioParadise)))

    }

    @Test
    fun `should retry in case first call fails`() {
        val double = RestClientDouble()
            .willReturn(invalidRP)
            .willReturn(validRP)

        val result = NowPlayingOnRadioParadise(double)()
        Assertions.assertThat(result).isEqualTo(Either.success<PlayerError, Song>(Song(artist = "AC/DC", title= "Highway to Hell", album = "Best Of AC/DC", source= Source.RadioParadise)))

    }

    @Test
    fun `should stop after two attempts`() {
        val double = RestClientDouble()
            .willReturn(invalidRP)
            .willReturn(invalidRP)
            .willReturn(validRP)

        val result = NowPlayingOnRadioParadise(double)()
        Assertions.assertThat(result).isEqualTo(Either.fail<RPError, Song>(RPError()))

    }
}


val validRP = """
    {
        "event": "2240942",
        "song": {
            "0": {
                "event": "2240942",
                "type": "M",
                "artist": "AC/DC",
                "title": "Highway to Hell",
                "album": "Best Of AC/DC"
            }
        },
        "current_event_id": 2240942
    }
""".trimIndent()

val invalidRP = """
    {
        "event": "2240942",
        "song": {
            "0": {
                "event": "2240942",
                "type": "M",
                "artist": "AC/DC",
                "title": "Highway to Hell",
                "album": "Best Of AC/DC"
            }
        },
        "current_event_id": 0
    }
""".trimIndent()
