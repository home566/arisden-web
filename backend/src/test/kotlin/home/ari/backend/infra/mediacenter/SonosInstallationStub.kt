package home.ari.backend.infra.mediacenter

import home.ari.backend.domain.annotations.DomainService
import home.ari.backend.domain.mediacenter.api.GetMediaCenter
import home.ari.backend.domain.mediacenter.model.Device
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.Room
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.domain.mediacenter.spi.Players
import home.ari.backend.shared.Either
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import java.lang.Thread.sleep


@DomainService
@Profile("stubbed")
@Primary
class SonosInstallationStub(private val players: Players): GetMediaCenter {

    val songs = listOf(
            Song(artist = "Elbow", album = "The Seldom seen kids", title = "Forget Myself", source= Source.Spotify),
            Song(artist = "Elbow", album = "The Seldom seen kids", title = "Audience with the pope", source= Source.Spotify),
            Song(artist = "Lord Huron", album = "Vide noir", title = "Lost in time and space", source= Source.Spotify),
            Song(artist = "The Kills", album = "", title = "Rodeo Town", source= Source.Spotify)
    )

    var i = 0

    override fun invoke(): MediaCenter {
        ++i

        return MediaCenter(listOf(
                Player(
                room = Room("Living Room"),
                device = Device("play-3"),
                song = songs[i % songs.size],
                state = PlayingState.Stopped
                ),
                Player(
                        room = Room("Bathroom"),
                        device = Device("play-1"),
                        song = songs[(i + 2) % songs.size],
                        state = PlayingState.Playing
                ),
                Player(
                        room = Room("Bedroom"),
                        device = Device("play-2"),
                        song = Song.nothing(),
                        state = PlayingState.Stopped
                )
        ))
    }
}
