package home.ari.backend.infra.mediacenter.features


import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.domain.mediacenter.rpSong
import home.ari.backend.domain.mediacenter.withSong
import home.ari.backend.domain.mediacenter.withState
import home.ari.backend.infra.TestContext
import home.ari.backend.infra.doubles.SonosPlayersMaker
import home.ari.backend.shared.Either
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


class MediaCenterGlue(
    val mockMvc: MockMvc,
    val context: TestContext,
    val sonosPlayersMaker: SonosPlayersMaker
) {

    var expectedMediaCenter = MediaCenter(listOf())

    @Before
    fun reset() {
        expectedMediaCenter = MediaCenter(listOf())
    }

    @Given("^my flat with a (.+)")
    fun my_flat_with(roomName: String) {
        configureNewRoom(roomName)
    }

    @Given("^with a (.+)")
    fun with_a(roomName: String) {
        configureNewRoom(roomName)
    }

    @Given("a new device now configured yet")
    fun a_new_device_now_configured_yet() {
        sonosPlayersMaker.createUnknownDevice()
    }

    @Given("{string} by {string} is playing in {string}")
    fun songPlayingIn(title: String, artist: String, room: String) {
        val song = Song(artist, "", title, Source.Spotify)

        sonosPlayersMaker.startSpotifyIn(room, song)

        val updated = expectedMediaCenter.players.map { player -> if (player.room.name == room) player.withState(Either.success(PlayingState.Playing)).withSong(song) else player }

        expectedMediaCenter = MediaCenter(updated)

    }

    @Given("Radio Paradise is playing in {string}")
    fun radio_paradise_is_playing_in(room: String) {
        sonosPlayersMaker.startRPIn(room)
        val updated = expectedMediaCenter.players.map { player -> if (player.room.name == room) player.withState(Either.success(PlayingState.Playing)).withSong(rpSong) else player }
        expectedMediaCenter = MediaCenter(updated)
    }

    @Given("the {string} device has been unplugged")
    fun the_device_has_been_unplugged(device: String) {
        sonosPlayersMaker.disablePlayer(device)
    }

    @Given("the {string} device does not respond")
    fun the_device_does_not_respond(device: String) {
        sonosPlayersMaker.busyPlayer(device)
    }

    @Given("RP does not sharing its metadata")
    fun rp_does_not_sharing_its_metadata() {
        sonosPlayersMaker.disableRP()
    }

    @When("I want to know which players are on")
    fun i_want_to_display_my_flat_plan() {
        context.r = mockMvc.perform(MockMvcRequestBuilders.get("/mediacenter"))
    }

    @When("I want to know what is playing in {string}")
    fun i_want_to_know_what_is_playing_in(room: String) {
        context.r = mockMvc.perform(MockMvcRequestBuilders.get("/mediacenter/now-playing/$room"))
    }

    @When("I want to control {string}")
    fun i_want_to_control(room: String) {
        context.r = mockMvc.perform(MockMvcRequestBuilders.get("/mediacenter/$room"))
    }

    @When("I play songs in {string}")
    fun i_play_songs_in(room: String) {
        context.r = mockMvc.perform(MockMvcRequestBuilders.post("/mediacenter/$room/play"))
    }

    @When("I stop the player in {string}")
    fun i_stop_the_player_in(room: String) {
        context.r = mockMvc.perform(MockMvcRequestBuilders.post("/mediacenter/$room/stop"))
    }

    @Then("The player in {string} should be off")
    fun the_player_in_should_be_off(room: String) {
        context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect( MockMvcResultMatchers.jsonPath("$[?(@.room == '$room')].state").value("Stopped"))
    }

    @Then("The player in {string} should be on")
    fun the_player_in_should_be_on(room: String) {
        context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect( MockMvcResultMatchers.jsonPath("$[?(@.room == '$room')].state").value("Playing"))
    }

    @Then("^I should be able to (.+) in \"(.+)\", now$")
    fun i_should_be_able_to_stop_in_now(action: String, room: String) {
        context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.room == '$room')].links[?(@.rel == '$action')].href").value("http://localhost/mediacenter/$room/$action"))
    }

    @Then("I should see all the controls for {string}")
    fun i_should_see_all_the_controls_for(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)

        result.andExpect {
            MockMvcResultMatchers.jsonPath("$.players[?(@.room == '$room')].links[?(@.rel == 'play')].href").isEmpty
        }
    }

    @Then("I should see all the players")
    fun i_should_see_all_the_players() {
        context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.players[*].room").value(containsInAnyOrder(*expectedMediaCenter.players.map { it.room.name }.toTypedArray())))
    }

    @Then("I should see {string} with no song")
    fun i_should_see_with_no_song(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)

        expectedMediaCenter.players.forEachIndexed {
            index, expected ->
            result.andExpect(MockMvcResultMatchers.jsonPath("$.players[$index].room").value(expected.room.name))
            if (room == expected.room.name) {
                result
                    .andExpect(MockMvcResultMatchers.jsonPath("$.players[$index].song.artist").value(""))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.players[$index].song.title").value(""))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.players[$index].song.album").value(""))
            }
        }
    }

    @Then("I should see Radio Paradise song in {string}")
    fun i_should_see_radio_paradise_song(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
        result.andExpect(MockMvcResultMatchers.jsonPath("$.artist").value(rpSong.artist))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(rpSong.title))
    }

    @Then("I should see {string} by {string}")
    fun i_should_see_by(song: String, artist: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
        result.andExpect(MockMvcResultMatchers.jsonPath("$.artist").value(artist))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(song))
    }

    @Then("I should see nothing is playing")
    fun i_should_see_nothing_is_playing() {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)
        result.andExpect(MockMvcResultMatchers.jsonPath("$.artist").value(""))
            .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(""))
    }

    @Then("I should have an error")
    fun i_should_have_an_error() {
        context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().`is`(object: BaseMatcher<Int>(){
            override fun describeTo(p0: Description?) {
                TODO("Not yet implemented")
            }

            override fun matches(status: Any): Boolean {
                return (status is Int) && status != HttpStatus.OK
            }

        }))
    }

    @Then("I should see all the players excepted {string}")
    fun i_should_see_all_the_players_excepted(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)

        MockMvcResultMatchers.jsonPath("$.players.length").value(expectedMediaCenter.players.size - 1)
        expectedMediaCenter.players.filter { it.room.name != room }.forEachIndexed {
            index, expected ->
            result
                .andExpect(MockMvcResultMatchers.jsonPath("$.players[$index].room").value(expected.room.name))
        }
    }

    @Then("I should be able to play in {string}")
    fun i_should_be_able_to_play_in(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)

        result.andExpect(
            MockMvcResultMatchers.jsonPath("$.players[?(@.room == '$room')].links[?(@.rel == 'play')].href").isNotEmpty
        )
    }

    @Then("I should not be able to play in {string}")
    fun i_should_not_be_able_to_play_in(room: String) {
        val result = context.r.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk)

        result.andExpect {
            MockMvcResultMatchers.jsonPath("$.players[?(@.room == '$room')].links[?(@.rel == 'play')].href").isEmpty
        }
    }

    private fun configureNewRoom(name: String) {
        sonosPlayersMaker.createPlayer(name)
        expectedMediaCenter = MediaCenter(expectedMediaCenter.players + Factories.player(name))
        /*sonosDiscoveryDouble.create(name)
        restClient.create(name)
        expectedMediaCenter = MediaCenter(expectedMediaCenter.players + Factories.player(name))

         */
    }
}
