package home.ari.backend.infra.doubles

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.Device
import home.ari.backend.infra.mediacenter.resources.sonos.discovery.SonosDiscovery

class SonosDiscoveryDouble: SonosDiscovery {

    val devices = mutableListOf<Device>()

    fun create(name: String) {
        devices.add(Factories.device(name))
    }

    override fun getAll(): List<Device> {
        return devices
    }

    fun reset() {
        devices.clear()
    }
}
