package home.ari.backend.shared.annotations

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

abstract class EntityTest<T> {
    abstract fun anEntity(): T
    abstract fun anIdenticalEntity(): T
    abstract fun anotherEntity(): T

    @Test
    fun `should consider two identical object as equal`() {
        assertThat(anEntity()).isEqualTo(anIdenticalEntity())
    }

    @Test
    fun `should consider two values with identical identity as equal`() {
        assertThat(anEntity()).isEqualTo(anIdenticalEntity())
    }

    @Test
    fun `should consider two values with different identities as not equal`() {
        assertThat(anEntity()).isNotEqualTo(anotherEntity())
    }

    @Test
    fun `should not be equal to another class instance`() {

    }
}
