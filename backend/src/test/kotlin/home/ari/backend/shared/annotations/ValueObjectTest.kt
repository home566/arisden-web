package home.ari.backend.shared.annotations

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

abstract class ValueObjectTest<T> {
    protected abstract fun aValue(): T
    protected abstract fun anotherValue(): T

    @Test
    fun `two identical values should be equals`() {
        assertThat(aValue()).isEqualTo(aValue())
    }

    @Test
    fun `two different values should be different`() {
        assertThat(aValue()).isNotEqualTo(anotherValue())
    }
}
