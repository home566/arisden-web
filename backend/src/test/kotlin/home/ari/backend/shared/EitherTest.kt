package home.ari.backend.shared

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

sealed class GeneralError
data class Error(val message: String="this is an error"): GeneralError()

sealed class GeneralValue
data class Value(val value: Int=12): GeneralValue()

class EitherTest {

    @Test
    fun `an error should be considered as it is`() {
        assertThat(error.isError()).isTrue
        assertThat(value.isError()).isFalse
    }

    @Test
    fun `should fold`() {
        assertThat(error.fold(
                { it},
                { Assertions.fail("Expecting an error but got the value") }
        )).isEqualTo(Error())
        assertThat(value.fold(
                { Assertions.fail("Expecting a value but got the error") },
                { it}
        )).isEqualTo(Value())
    }

    @Test
    fun `should map`() {
        assertThat(error.map<Double, Double>(
                { 1.2 },
                { Assertions.fail("Expecting an error but got the value") }
        )).isEqualTo(Either.fail<Double, Value>(1.2))
        assertThat(value.map<Double, Double>(
                { Assertions.fail("Expecting an error but got the error") },
                { 1.2 }
        )).isEqualTo(Either.success<Error, Double>(1.2))
    }

    @Test
    fun `should display`() {
        assertThat(error.toString()).isEqualTo("Error{Error(message=this is an error)}")
        assertThat(value.toString()).isEqualTo("Value{Value(value=12)}")
    }

    @Test
    fun `should generify`() {
        var generified: Either<GeneralError, GeneralValue> = error.generify()
        assertThat(generified).isEqualTo(Either.fail<GeneralError, GeneralValue>(Error()))
        generified = value.generify()
        assertThat(generified).isEqualTo(Either.success<GeneralError, GeneralValue>(Value()))
    }

    @Test
    fun `should merge values`() {
        assertThat(listOf(
                Either.success<Error, Value>(Value(1)),
                Either.success<Error, Value>(Value(2)),
                Either.success<Error, Value>(Value(3)),
                Either.success<Error, Value>(Value(4))
        ).merge()).isEqualTo(Either.success<Error, List<Value>>(listOf(Value(1), Value(2), Value(3), Value(4))))
    }

    @Test
    fun `should stop merge on error`() {
        assertThat(listOf(
                Either.success<Error, Value>(Value(1)),
                Either.success<Error, Value>(Value(2)),
                Either.fail<Error, Value>(Error()),
                Either.success<Error, Value>(Value(4))
        ).merge()).isEqualTo(Either.fail<Error, List<Value>>(Error()))
    }

    @Test
    fun `should map a value`() {
        assertThat(value.mapOnSuccess { Either.success<GeneralError, Double>(1.2) })
            .isEqualTo(Either.success<GeneralError, Double>(1.2))
    }

    @Test
    fun `mapValue should return the error`() {
        assertThat(error.mapOnSuccess { Either.success<GeneralError, Double>(1.2) })
            .isEqualTo(error)
    }

    @Test
    fun `should apply on success`() {
        value.onSuccess {
            value -> assertThat(value).isEqualTo(Value(12))
        }
        .onFailure {
                fail("Should not call onFailure when it's a value")
        }
    }

    @Test
    fun `should apply on failure`() {
        error.onSuccess {
            fail("Should not call onSuccess when it's an error")
        }
            .onFailure {
                value -> assertThat(value).isEqualTo(Error("this is an error"))
            }
    }


    private val error = Either.fail<Error, Value>(Error("this is an error"))
    private val value = Either.success<Error, Value>(Value(12))
}
