package home.ari.backend.shared.annotations

import org.junit.jupiter.api.*
import java.lang.reflect.Method

/*
If we act non-virtuously suffering results

If we have to spend more than 1 minute to understand a problem linked to a code that is not fully covered by the tests (ex: the equals/hashCode methods),
we should re-write this code using TDD and make sure that all the cases are covered by a test
*/

@Retention(AnnotationRetention.RUNTIME)
@DisplayNameGeneration(Foo::class)
@Target(AnnotationTarget.CLASS)
@Tag("badKarma")
annotation class BadKarmaTest



class Foo: DisplayNameGenerator {
    val moto = "If we act non-virtuously, suffering results"
    override fun generateDisplayNameForClass(cls: Class<*>): String {
        return "${cls.simpleName}: $moto"
    }

    override fun generateDisplayNameForNestedClass(cls: Class<*>): String {
        return "${cls.simpleName}: $moto"
    }

    override fun generateDisplayNameForMethod(cls: Class<*>, mthd: Method): String {
        return "${mthd.name}: $moto"
    }

}
