package home.ari.backend.hexarch

import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import com.tngtech.archunit.lang.syntax.elements.ClassesThat


@AnalyzeClasses(packages = ["home.ari.backend"], importOptions = [ImportOption.DoNotIncludeTests::class])
class ConsistencyTest {
    @ArchTest
    val domain_should_not_depends_on_infra: ArchRule = noClasses().that().resideInAPackage("home.ari.backend.domain..")
        .should().dependOnClassesThat().resideInAPackage("home.ari.backend.infra..")

    @ArchTest
    val domain_should_be_pure: ArchRule = noClasses().that().resideInAPackage("home.ari.backend.domain..")
        .should().dependOnClassesThat().areOutsideOfProjectPackages("domain")

    @ArchTest
    val flatplan_should_not_depends_on_other_domains: ArchRule = noClasses().that().resideInAProjectPackage("flatplan")
        .should().dependOnClassesThat().otherDomainPackagesThan("flatplan")

    @ArchTest
    val mediacenter_should_not_depends_on_other_domains: ArchRule = noClasses().that().resideInAProjectPackage("mediacenter")
        .should().dependOnClassesThat().otherDomainPackagesThan("mediacenter")

}

private fun <CONJUNCTION> ClassesThat<CONJUNCTION>.resideInAProjectPackage(p: String): CONJUNCTION {
    return this.resideInAPackage("home.ari.backend.domain.$p..")
}

private fun <CONJUNCTION> ClassesThat<CONJUNCTION>.areOutsideOfProjectPackages(vararg modules: String): CONJUNCTION {
    val packages = modules.map { "home.ari.backend.$it.." }.toTypedArray()
    return this.resideOutsideOfPackages("kotlin..", "java..", "org.jetbrains.annotations..", "home.ari.backend.shared..", "home.ari.backend.domain.annotations", *packages)
}

private fun <CONJUNCTION> ClassesThat<CONJUNCTION>.otherDomainPackagesThan(vararg modules: String): CONJUNCTION {
    return this.areOutsideOfProjectPackages(*modules.map { "domain.$it" }.toTypedArray())
}
