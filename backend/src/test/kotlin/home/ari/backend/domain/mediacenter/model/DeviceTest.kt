package home.ari.backend.domain.mediacenter.model

import home.ari.backend.shared.annotations.ValueObjectTest

class DeviceTest: ValueObjectTest<Device>() {
    override fun aValue(): Device = Device("127.0.0.1")

    override fun anotherValue(): Device = Device("192.168.0.12")
}
