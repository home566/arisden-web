package home.ari.backend.domain.mediacenter.model

import home.ari.backend.shared.annotations.BadKarmaTest
import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.RoomDoesNotHavePlayerError
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test


class PlayerErrorsTest {

    @Test
    fun `two errors are the same if they have the same type and message`() {
        Assertions.assertThat(
                RoomDoesNotHavePlayerError(Factories.room("bedroom"))
        ).isEqualTo(
                RoomDoesNotHavePlayerError(Factories.room("bedroom"))
        )
    }

    @Test
    fun `two errors are different if they haven't the same message`() {
        Assertions.assertThat(
                RoomDoesNotHavePlayerError(Factories.room("living room"))
        ).isNotEqualTo(
                RoomDoesNotHavePlayerError(Factories.room("bedroom"))
        )
    }

    @Test
    fun `two errors are different if they haven't the same type`() {
        Assertions.assertThat(
                RoomDoesNotHavePlayerError(Factories.room("bedroom"))
        ).isNotEqualTo(
                IllegalArgumentException()
        )
    }
}
