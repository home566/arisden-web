package home.ari.backend.domain.mediacenter.doubles

import home.ari.backend.domain.mediacenter.model.media.Song

data class DeviceDeclaration(val name: String, val song: Song = Song.nothing(), val disabled: Boolean = false) {
    fun with(song: Song): DeviceDeclaration {
        return copy(song = song)
    }

    fun unplugged(): DeviceDeclaration {
        return copy(disabled = true)
    }
}
