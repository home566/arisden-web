package home.ari.backend.domain.flatplan

import home.ari.backend.domain.flatplan.doubles.PlansDouble
import home.ari.backend.domain.flatplan.doubles.RoomPositionsDouble
import home.ari.backend.domain.flatplan.model.Position
import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.domain.flatplan.services.PlanGetter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GetPlanTest {
    @Test
    fun `should retrieve a plan by its name`() {
        `in a flat named`("sweet home").with(
                "Bathroom".at(0, 0).of(4 x 4),
                "Living room".at(4, 0).of(8 x 4),
                "Kitchen".at(4, 4).of(2 x 4),
                "Bedroom".at(6, 4).of(6 x 4),
        ).`the configuration should be`(
                Room("Bathroom", listOf(Position(0.2, 0.2)), Position(0.2, 0.2)),
                Room("Living room", listOf(Position(0.8, 0.2)), Position(0.8, 0.2)),
                Room("Kitchen", listOf(Position(0.5, 0.6)), Position(0.5, 0.6)),
                Room("Bedroom", listOf(Position(0.9, 0.6)), Position(0.9, 0.6))
        )
    }


    private infix fun Int.x(y: Int): Position {
        return Position(this.toDouble(), y.toDouble())
    }

    private fun String.at(x: Int, y: Int): RoomHelper {
        return RoomHelper(this, Position(x.toDouble(), y.toDouble()))
    }

    private fun `in a flat named`(name: String): FlatPlanTestHelper {
            return FlatPlanTestHelper(name)
    }
}

class FlatPlanTestHelper(val name: String) {
    lateinit var rooms: List<RoomHelper>
    fun with(vararg roomHelpers: RoomHelper): FlatPlanTestHelper {
        rooms = roomHelpers.toList()
        return this
    }
    fun `the configuration should be`(vararg expected: Room) {
        val result = PlanGetter(RoomPositionsDouble(rooms.map { it.room() }.toMutableList())).named(name)

        Assertions.assertEquals(expected.toList(), result.rooms)
    }

}


class RoomHelper(val name: String, val position: Position) {
    lateinit var center: Position

    fun of(p: Position): RoomHelper {
        this.center = Position(position.x + p.x/2, position.y + p.y/2)
        return this
    }

    fun room(): Room {
        return Room(name, listOf(center), center)
    }
}
