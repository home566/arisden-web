package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.shared.annotations.ValueObjectTest

class InstructionTest: ValueObjectTest<Instruction>() {
    override fun aValue(): Instruction = Instruction(Factories.device("room"), Action.Play)
    override fun anotherValue(): Instruction = Instruction(Factories.device("room"), Action.Stop)
}
