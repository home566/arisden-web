package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class MediaCenterTest {

    val mediaCenter = Factories.mediaCenter("living room", "office", "bathroom", "bedroom")

    @Test
    fun `should return true if a room has a player`() {
        Assertions.assertThat(
                mediaCenter.`has room named`("living room")
        ).isTrue
    }

    @Test
    fun `should return false if a room does not have a player`() {
        Assertions.assertThat(
                mediaCenter.`has room named`("kitchen")
        ).isFalse
    }

    @Test
    fun `should return the song playing in a room`() {
        Assertions.assertThat(
                mediaCenter.`where a song is playing in`("living room")
                    .`song playing in`(Factories.room("living room"))
        ).isEqualTo(
                Either.success<PlayerError, Song>(spotifySong)
        )
    }

    @Test
    fun `should return an error when asking the song in the wrong room`() {
        Assertions.assertThat(
                mediaCenter
                    .`song playing in`(Factories.room("kitchen"))
        ).isEqualTo(
                Either.fail<PlayerError, Song>(RoomDoesNotHavePlayerError(Factories.room("kitchen")))
        )
    }

    @Test
    fun `should start a player in the given room`() {
        mediaCenter.play(Factories.room("living room")).fold(
                {Assertions.fail<Unit>("Should configure the player but got the error $it")},
                {
                    Assertions.assertThat(it.takePendingInstructions()).contains(
                            Instruction(Factories.device("living room"), Action.Play)
                    )
                }
        )
    }

    @Test
    fun `should return an error when trying to start a player in the wrong room`() {
        Assertions.assertThat(mediaCenter.play(Factories.room("kitchen")))
            .isEqualTo(Either.fail<PlayerError, Unit>(RoomDoesNotHavePlayerError(Factories.room("kitchen"))))
    }
}
