package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.doubles.PlayersDouble
import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.services.SonosInstallation
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ConfigureMediaCenterTest {
    @Test
    fun `should apply instructions`() {
        val instructions = Instructions() +
                Instruction(Factories.device("bedroom"), Action.Play) +
                Instruction(Factories.device("bathroom"), Action.Stop)

        val sut = SonosInstallation(PlayersDouble(listOf(Factories.declare("bedroom"),
                                                         Factories.declare("bathroom"))))

        val result = sut(instructions)

        assertThat(result).isEqualTo(Either.success<PlayerError, List<Player>>(listOf(Factories.player("bedroom"), Factories.player("bathroom"))))

    }
}
