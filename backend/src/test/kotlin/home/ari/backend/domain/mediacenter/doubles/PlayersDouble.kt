package home.ari.backend.domain.mediacenter.doubles

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.spi.Players
import home.ari.backend.domain.mediacenter.spi.UpdatedPlayers
import home.ari.backend.shared.Either
import kotlin.reflect.KProperty

class PlayersDouble(devices: List<DeviceDeclaration>): Players {

    private val states = mutableMapOf<String, PlayingState>()


    private val players: List<Player> = devices.map {

        (device, song, disabled) ->

        states[device] = PlayingState.Stopped
        Player(Factories.room(device), Factories.device(device),
                Either.success(song),
                if (disabled) Either.fail(PlayerIsNotResponding(Device(device))) else Either.success(states[device]!!))
    }

    override fun getAll(): List<Player> {
        return players
    }

    override fun configure(instructions: Instructions): Either<PlayerError, UpdatedPlayers> {
        val players = mutableListOf<Player>()
        var error: PlayerError? = null
        instructions.forEach {  instruction ->
            val player = this.players.find { it.device == instruction.device }
            if (player != null) {
                when(instruction.command) {
                    Action.Play -> players.add(player.playing())
                    Action.Stop -> players.add(player.stopped())
                }
            } else if (error == null) {
                error = PlayerIsNotResponding(instruction.device)
            }
        }

        if (error != null) {
            return Either.fail(error!!)
        }
        return Either.success(players)
    }

    /*fun songAccess(song: Song, room: String): Either<PlayerError, Song> {
        val u: Either<PlayerError, Song> by object {
            operator fun getValue(nothing: Nothing?, property: KProperty<*>): Either<PlayerError, Song> {
                return Either.value(song)
            }
        }
        return u
    }*/
}

private fun Player.playing(): Player {
    return Player(
            room = room,
            device = device,
            song = song,
            state = Either.success(PlayingState.Playing))
}
private fun Player.stopped(): Player {
    return Player(
            room = room,
            device = device,
            song = song,
            state = Either.success(PlayingState.Playing))
}

fun <T> d(f: () -> T): T {
    val u: T by object {
        operator fun getValue(nothing: Nothing?, property: KProperty<*>): T {
            return f()
        }
    }
    return u
}
