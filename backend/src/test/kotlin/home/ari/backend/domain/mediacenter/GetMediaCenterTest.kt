package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class GetMediaCenterTest {

    fun `in my flat`(init: FlatHelper.() -> Unit): FlatHelper {
        val flat = `in a flat with` {
            a("Bathroom")
            a("Living Room")
            a("Room Office")
        }
        flat.init()
        return flat
    }

    @Test
    fun `should return all rooms`() {
        `in my flat` {}
        .`the configuration should be`(
                "Bathroom",
                "Living Room",
                "Room Office"
        )
    }

    @Test
    fun `should find a room by its name`() {
        `in my flat` {}
        .`check media center` {
            `has room named`("Living room").`should be`(true)
        }

    }

    @Test
    fun `should not find an unknown room`() {
        `in my flat` {}
        .`check media center` {
            `has room named`("bedroom").`should be`(false)
        }
    }

    @Test
    fun `should find the song playing in a room`() {
        `in my flat` {
            "Something to believe in" by "King Creosote" `is playing in` "Living Room"
        }
        .`check media center` {
            `song playing in`(Room("Living Room"))
                .`should be`("Something to believe in" by "King Creosote")
        }
    }

    @Test
    fun `should raise an error when getting song from a room that does not exist`() {
        `in my flat` {
            "Something to believe in" by "King Creosote" `is playing in` "Living Room"
        } `check media center` {
                `song playing in`(Room("bedroom")).`should be`(RoomDoesNotHavePlayerError(Room("bedroom")))
            }
    }

    @Test
    fun `should play song in a room`() {
        `in my flat` {
        }
        .`configure media center as` {
            play(Factories.room("Living Room"))
        }.`should contains`(
            Instruction(Factories.device("Living Room"), Action.Play)
        )
    }

    @Test
    fun `should raise an error when playing in a room without player`() {
        `in my flat` {
        }
            .`check media center` {
                play(Room("bedroom"))
                    .`should raise an error`(RoomDoesNotHavePlayerError(Room("bedroom")))
            }
    }
}


private fun <E, V> Either<E, V>.`should give`(expected: V) {
    this.fold(
            {Assertions.fail<E>("Expected a value but got the error '$it'")},
            {assertThat(it).isEqualTo(expected)}
    )
}

private fun <E, V> Either<E, V>.`should raise an error`(expected: E) {
    this.fold(
            {assertThat(it).isEqualTo(expected)},
            {Assertions.fail<E>("Expected an error but got the value '$it'")},
    )
}
