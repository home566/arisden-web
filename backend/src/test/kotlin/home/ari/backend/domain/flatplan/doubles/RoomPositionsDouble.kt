package home.ari.backend.domain.flatplan.doubles

import home.ari.backend.domain.flatplan.model.Room
import home.ari.backend.domain.flatplan.spi.RoomPositions
import org.springframework.context.annotation.Primary


class RoomPositionsDouble(val rooms: MutableList<Room> = mutableListOf()): RoomPositions {
    override fun `in`(plan: String): List<Room> {
        return rooms
    }
}
