package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.model.Action
import home.ari.backend.domain.mediacenter.model.Instruction
import home.ari.backend.domain.mediacenter.model.Instructions
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class InstructionTest {

    @Test
    fun `two equivalent instructions should concerned the same device`() {
        Assertions.assertThat(Instruction(
                Factories.device("room"), Action.Play) `is overridden by`
                Instruction(Factories.device("room"), Action.Play)
        ).isTrue

        Assertions.assertThat(
                Instruction(Factories.device("room"), Action.Play) `is overridden by`
                Instruction(Factories.device("another room"), Action.Play)
        ).isFalse
    }

    @Test
    fun `should add an instruction`() {
        val instructions = Instructions() + Instruction(Factories.device("room"), Action.Play)
        Assertions.assertThat(instructions).containsExactly(Instruction(Factories.device("room"), Action.Play))
    }

    @Test
    fun `should remove instruction discarded by the last added`() {
        val instructions = Instructions() +
                Instruction(Factories.device("room"), Action.Play) +
                Instruction(Factories.device("room"), Action.Stop)
        Assertions.assertThat(instructions).containsExactly(Instruction(Factories.device("room"), Action.Stop))
    }

    @Test
    fun `should remove only instructions concerning the device`() {
        val instructions = Instructions() +
                Instruction(Factories.device("room"), Action.Play) +
                Instruction(Factories.device("room bis"), Action.Play) +
                Instruction(Factories.device("room ter"), Action.Play) +
                Instruction(Factories.device("room"), Action.Stop)

        Assertions.assertThat(instructions).containsExactly(
                Instruction(Factories.device("room bis"), Action.Play),
                Instruction(Factories.device("room ter"), Action.Play),
                Instruction(Factories.device("room"), Action.Stop)
        )
    }
}
