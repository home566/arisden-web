package home.ari.backend.domain.flatplan.model

import home.ari.backend.shared.annotations.ValueObjectTest

class PositionTest: ValueObjectTest<Position>() {
    override fun aValue(): Position {
        return Position(20.0, 34.0)
    }

    override fun anotherValue(): Position {
        return Position(12.0, 56.0)
    }
}
