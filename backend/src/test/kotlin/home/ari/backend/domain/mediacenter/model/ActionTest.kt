package home.ari.backend.domain.mediacenter.model

import home.ari.backend.shared.annotations.ValueObjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ActionTest {

    @Test
    fun `should convert a string to object`() {
        assertThat(Action.asAction("Stop")).isEqualTo(Action.Stop)
        assertThat(Action.asAction("Play")).isEqualTo(Action.Play)
    }

    @Test
    fun `should compare two identical actions`() {
        assertThat(Action.asAction("Stop").`is same kind of`(Action.Stop)).isTrue
    }
}
