package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.model.Player
import home.ari.backend.domain.mediacenter.model.PlayerIsNotResponding
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class PlayerTest {

    val player = Player(
            Factories.room("player"),
            Factories.device("player"),
            song = Either.success(Factories.song()),
            state = Either.success(PlayingState.Stopped)
    )

    @Test
    fun `should be ready to play if stopped and a song is selected`() {
        Assertions.assertThat(player.readyToPlay()).isTrue
    }

    @Test
    fun `should not be ready to play if already playing`() {
        Assertions.assertThat(
                player.withState(Either.success(PlayingState.Playing)).readyToPlay()
        ).isFalse
    }

    @Test
    fun `should not be ready to play if the device does not respond`() {
        Assertions.assertThat(
                player.withState(Either.fail(PlayerIsNotResponding(player.device))).readyToPlay()
        ).isFalse
    }
}
