package home.ari.backend.domain.mediacenter.model

import home.ari.backend.shared.annotations.BadKarmaTest
import home.ari.backend.domain.mediacenter.Factories
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

@BadKarmaTest
class PlayerTest {

    @Test
    fun `two players should be equals if they have the same device and room`() {
        Assertions.assertThat(Factories.player("bedroom"))
            .isEqualTo(Factories.player("bedroom"))
    }

    @Test
    fun `two players should be different if they are not in the same room`() {
        Assertions.assertThat(Factories.player("bedroom"))
            .isNotEqualTo(Factories.player("Living room", device = "bedroom"))
    }

    @Test
    fun `two players should be different if they are not linked to the same device`() {
        Assertions.assertThat(Factories.player("bedroom"))
            .isNotEqualTo(Factories.player("bedroom", device = "Living room"))
    }

    @Test
    fun `a player should be different of any object from another class`() {
        Assertions.assertThat(Factories.player("living room")).isNotEqualTo(object {val name = "an object"})
    }
}
