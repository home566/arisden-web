package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.doubles.DeviceDeclaration
import home.ari.backend.domain.mediacenter.doubles.PlayersDouble
import home.ari.backend.domain.mediacenter.model.Instruction
import home.ari.backend.domain.mediacenter.model.Instructions
import home.ari.backend.domain.mediacenter.model.MediaCenter
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.domain.mediacenter.services.SonosInstallation
import home.ari.backend.shared.Either
import org.assertj.core.api.Assertions

class FlatHelper {
    private val devices = mutableListOf<DeviceDeclaration>()

    fun `the configuration should be`(vararg roomNames: String) {
        val roomsDouble = PlayersDouble(devices.toList())
        val getFlatMediaCenter = SonosInstallation(roomsDouble)
        val result = getFlatMediaCenter()

        Assertions.assertThat(result.players.sortedBy { it.room.name })
            .containsExactlyInAnyOrder(*(roomNames.sorted().map {
                val device = devices.first { d -> d.name == it }.name
                Factories.player(it, device=device)
            }).toTypedArray())
    }

    infix fun `check media center`(test: MediaCenter.() -> Unit) {
        val roomsDouble = PlayersDouble(devices.toList())
        val getFlatMediaCenter = SonosInstallation(roomsDouble)
        getFlatMediaCenter().test()
    }

    infix fun `configure media center as`(configure: MediaCenter.() -> Unit): Instructions {
        val roomsDouble = PlayersDouble(devices.toList())
        val getFlatMediaCenter = SonosInstallation(roomsDouble)
        val mediaCenter = getFlatMediaCenter()
        mediaCenter.configure()
        return mediaCenter.takePendingInstructions()
    }

    infix fun a(room: String) {
        val d = DeviceDeclaration(room)
        devices.add(d)
    }

    infix fun Song.`is playing in`(room: String) {
        val room = devices.firstOrNull { it.name == room }!!
        devices.remove(room)
        devices.add(room.with(this))
    }
}

fun <T> Any.`should be`(expected: T) {
    Assertions.assertThat(this).isEqualTo(expected)
}

fun Instructions.`should contains`(vararg expected: Instruction) {
    Assertions.assertThat(this.pending).containsExactlyInAnyOrder(*expected)
}

fun <E, V> Either<E, V>.`should be`(expected: Any) {
    if (this.isError())
        Assertions.assertThat(this).isEqualTo(Either.fail<E, V>(expected as E))
    else
        Assertions.assertThat(this).isEqualTo(Either.success<E, V>(expected as V))
}

infix fun String.by(artist: String): Song {
    return Song(artist = artist, title = this, album = "Best Of", source = Source.Spotify)
}

fun `in a flat with`(init: FlatHelper.() -> Unit): FlatHelper {
    val f = FlatHelper()
    f.init()
    return f
}
