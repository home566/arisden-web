package home.ari.backend.domain.mediacenter.model

import home.ari.backend.domain.mediacenter.Factories
import home.ari.backend.shared.annotations.ValueObjectTest

class InstructionsTest: ValueObjectTest<Instructions>() {
    override fun aValue(): Instructions =
        Instructions() + Instruction(Factories.device("room"), Action.Play)

    override fun anotherValue(): Instructions =
        Instructions() + Instruction(Factories.device("room"), Action.Stop)
}
