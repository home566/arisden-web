package home.ari.backend.domain.flatplan.model

import home.ari.backend.shared.annotations.BadKarmaTest
import home.ari.backend.domain.flatplan.Factories
import home.ari.backend.shared.annotations.EntityTest
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

@BadKarmaTest
class RoomTest: EntityTest<Room>() {
  override fun anEntity(): Room {
        return Room(
            name = "living room",
            angles = listOf(Position(10.0, 10.0), Position(20.0, 10.0), Position(15.0, 20.0)),
            playerPosition = Position(15.0, 15.0)
        )
    }

    override fun anIdenticalEntity(): Room {
        return Room(
                name = "living room",
                angles = listOf(Position(5.0, 400.0), Position(200.0, 10.0), Position(15.0, 200.0)),
                playerPosition = Position(150.0, 150.0)
        )
    }

    override fun anotherEntity(): Room {
        return Room(
                name = "bathroom",
                angles = listOf(Position(5.0, 400.0), Position(200.0, 10.0), Position(15.0, 200.0)),
                playerPosition = Position(150.0, 150.0)
        )
    }

    @Test
    fun `the name should be case insensitive`() {
        assertThat(
                Room(
                        name = "living room",
                        angles = listOf(Position(5.0, 400.0), Position(200.0, 10.0), Position(15.0, 200.0)),
                        playerPosition = Position(150.0, 150.0)
                )
        ).isEqualTo(
                Room(
                        name = "LIVING ROOM",
                        angles = listOf(Position(5.0, 400.0), Position(200.0, 10.0), Position(15.0, 200.0)),
                        playerPosition = Position(150.0, 150.0)
                )
        )
    }
}
