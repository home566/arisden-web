package home.ari.backend.domain.flatplan

import home.ari.backend.domain.flatplan.model.Position
import home.ari.backend.domain.flatplan.model.Room

class Factories {
    companion object {
        fun room(name: String): Room {
            return Room(name, listOf(Position(10.0, 10.0), Position(20.0, 10.0), Position(20.0, 20.0), Position(10.0, 20.0)), Position(15.0, 15.0))
        }
    }
}
