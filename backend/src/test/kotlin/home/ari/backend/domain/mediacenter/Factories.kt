package home.ari.backend.domain.mediacenter

import home.ari.backend.domain.mediacenter.doubles.DeviceDeclaration
import home.ari.backend.domain.mediacenter.model.*
import home.ari.backend.domain.mediacenter.model.media.PlayingState
import home.ari.backend.domain.mediacenter.model.media.Song
import home.ari.backend.domain.mediacenter.model.media.Source
import home.ari.backend.shared.Either
import kotlin.math.abs

class Factories {
    companion object {
        fun device(name: String): Device {
            val addr = "127.0.0.${abs(name.hashCode()) % 255}"
            return Device(addr)
        }

        fun room(name: String): Room {
            return Room(name)
        }

        fun player(room: String, device: String = room, song: Song = Song.nothing()): Player {
            return Player(
                    room(room),
                    device(device),
                    song,
                    PlayingState.Stopped
            )
        }

        fun player(room: String, device: String = room, song: Either<PlayerError, Song>): Player {
            return Player(
                    room(room),
                    device(device),
                    song,
                    Either.success(PlayingState.Stopped)
            )
        }

        fun song(): Song {
            return spotifySong
        }

        fun mediaCenter(vararg rooms: String): MediaCenter {
            return MediaCenter(
                    rooms.map { player(it) }
            )
        }

        fun declare(name: String): DeviceDeclaration {
            return DeviceDeclaration(name)
        }
    }
}

val rpSong = Song("AC/DC", "Highway to Hell", "Highway to Hell", Source.RadioParadise)
val spotifySong = Song(artist = "Elbow", album = "The Seldom seen kids", title = "Forget Myself", source= Source.Spotify)

fun Player.withState(value: Either<PlayerError, PlayingState>): Player {
    return Player(room, device, song, value)
}

fun Player.withState(value: PlayingState): Player {
    return Player(room, device, song, Either.success(value))
}

fun Player.withSong(song: Song): Player {
    return Player(room, device, Either.success(song), state)
}


fun MediaCenter.`where a song is playing in`(room: String): MediaCenter {
    val playerWithSong = this.players.first { it.room.name == room }
    val players = this.players.filter { it != playerWithSong } + playerWithSong.withState(Either.success(PlayingState.Playing)).withSong(
            spotifySong
    )

    return MediaCenter(
            players
    )
}
