Feature: Media Center

  Background:
    Given my flat with a living room
    And with a bedroom
    And with a office room
    And with a bathroom
    And "Something believe in" by "King Creosote" is playing in "bathroom"
    And Radio Paradise is playing in "living room"

  Scenario: List all players
    When I want to know which players are on
    Then I should see all the players

  Scenario: Get a player details
    When I want to control "bathroom"
    Then I should see all the controls for "bathroom"

  Scenario: Get a error when getting details of an unknown player
    When I want to control "kitchen"
    Then I should have an error

  Scenario: Unplugged device should not be displayed
    Given a new device now configured yet
    When I want to know which players are on
    Then I should see all the players

  Scenario: Display the song playing in the player
    When I want to know what is playing in "bathroom"
    Then I should see "Something believe in" by "King Creosote"

  Scenario: Display the radio song playing in the player
    When I want to know what is playing in "living room"
    Then I should see Radio Paradise song in "living room"

  Scenario: Display nothing if RP is not responding
    Given RP does not sharing its metadata
    When I want to know what is playing in "living room"
    Then I should see nothing is playing

  Scenario: Display nothing if the player has no playlist
    When I want to know what is playing in "bedroom"
    Then I should see nothing is playing

  Scenario: Remove the player if the device has been unplugged
    Given the "bathroom" device has been unplugged
    When I want to know which players are on
    Then I should see all the players excepted "bathroom"

  Scenario: Display nothing if the device is busy
    Given the "bathroom" device does not respond
    When I want to know which players are on
    Then I should see "bathroom" with no song

  Scenario: Display an error if the room does not exist
    When I want to know what is playing in "kitchen"
    Then I should have an error

  Scenario: Allow to play on a player
    When I want to know which players are on
    Then I should see all the players
    And I should be able to play in "bedroom"
    And I should not be able to play in "office room"

  Scenario: Play on a player
    When I play songs in "bedroom"
    Then The player in "bedroom" should be on
    And I should be able to stop in "bedroom", now

  Scenario: Cannot play on an unknown player
    When I play songs in "garage"
    Then I should have an error


  Scenario: Stop a player
    When I stop the player in "bathroom"
    Then The player in "bathroom" should be off
    And I should be able to play in "bathroom", now
