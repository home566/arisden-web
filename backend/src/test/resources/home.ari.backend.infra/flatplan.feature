Feature: Flat Plan

  Background:
    Given my "home" with:
    |Living room| 0;0 10;0 10;20 0;20    |
    |Kitchen    | 0;20 10;20 10;30 0;30  |
    |Bedroom    | 10;0 15;0 15;15 10;15  |
    |Bath room  | 10;15 15;0 15;30 10;30 |
    And I have players in:
      |Living room|
      |Bedroom    |
      |Bath room  |

    Scenario: List all rooms
      When I want to display my flat plan
      Then I should see my rooms

    Scenario: A room with a player should define the way to retrieve metadata
      When I want to display my flat plan
      Then I should know how to retrieve the metadata in "Living room"
