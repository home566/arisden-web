import RoomDetails from "./RoomDetails";
import Song from "./Song";
import PlayerLinks from "./PlayerLinks";
import {noLinks} from "../services/PlayerService";

export class RoomInformation {
    room: RoomDetails;
    song: Song | null;
    links: PlayerLinks;

    constructor(room: RoomDetails, song: Song | null=null, links: PlayerLinks=noLinks) {
        this.room = room;
        this.song = song;
        this.links = links;
    }
}
