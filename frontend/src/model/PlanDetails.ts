import RoomDetails from "./RoomDetails";

export default interface PlanDetails {
    plan: string,
    rooms: RoomDetails[],
}
