import Song from "./Song";
import PlayerLinks from "./PlayerLinks";

export default interface PlayerDetails {
    room: string,
    song: Song,
    state: string
    links: PlayerLinks
}
