export default interface Song {
    artist: string,
    title: string,
    album: string
}
