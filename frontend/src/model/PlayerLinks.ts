


export default interface PlayerLinks {
    play: string | null,
    stop: string | null,

    previous: string | null,
    next: string | null
}
