export default class Rect {
    left: number;
    top: number;
    width: number;
    height: number;
    bottom: number;
    right: number;

    constructor(left: number,  top: number, width: number, height: number) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
        this.bottom = top + height;
        this.right = left + width;
    }
}
