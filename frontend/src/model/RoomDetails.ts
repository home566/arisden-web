import {Point} from "./Point";

export default interface RoomDetails {
    name: string,
    angles: Point[],
    playerInformationUrl: string | null,
    playerPosition: Point
}
