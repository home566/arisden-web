import * as d3 from "d3";
import {RoomInformation} from "../../model/RoomInformation";
import {Point} from "../../model/Point";
import Rect from "../../model/Rect";
import Player from "./Player";
import PlayerLinks from "../../model/PlayerLinks";
import ReactDOMServer from "react-dom/server";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";

const makePlayer = (links: PlayerLinks) => {
    return ReactDOMServer.renderToString(<Player links={links} />);
};


const call = (url: string) => {
    fetch(url).then(res =>
        {
            if (!res.ok) {
                throw res.status;
            }
            return res.json()
        }
    );
}


export function makeRoom(info: RoomInformation, svgElement: d3.Selection<SVGSVGElement, unknown, null, undefined>) {
    const room = info.room;
    const links = info.links;
    const song = info.song;
    const boundingRect = boundingRectOf(room.angles);
    const x = room.playerPosition.x;
    const y = room.playerPosition.y;
    svgElement.append("polygon")
        .attr("points", room.angles.map((p) => `${p.x} ${p.y}`).join(', '))
        .attr("fill", room.playerInformationUrl ? "white" : "darkgray")
        .attr("stroke", "#575757")
        .attr("stroke-width", "0.5")
        .attr("vector-effect", "non-scaling-stroke")
        .attr('filter', "url(#f3)");
    svgElement.append("text")
        .attr("id", room.name)
        .attr("x", boundingRect.left + 4)
        .attr("y", boundingRect.top + 2)
        .attr("width", "46")
        .attr("dominant-baseline", "hanging")
        .attr("text-anchor", "start")
        .attr("font-size", "12%")
        .attr("fill", "darkgray")
        .text(room.name);
    if (song) {
        svgElement.append("text")
            .attr("id", room.name)
            .attr("x", x)
            .attr("y", y)
            .attr("width", "46")
            .attr("dominant-baseline", "hanging")
            .attr("text-anchor", "start")
            .attr("font-size", "18%")
            .attr("fill", "#202020")
            .text(song.title);
        svgElement.append("text")
            .attr("id", room.name)
            .attr("x", x)
            .attr("y", y + 4)
            .attr("width", "32")
            .attr("dominant-baseline", "hanging")
            .attr("text-anchor", "start")
            .attr("font-size", "18%")
            .attr("fill", "darkgray")
            .text(song.artist);
        const g = svgElement.append("g");
        g.attr("transform", `translate(${boundingRect.left + 4} ${boundingRect.top + 4}) scale(0.2 0.2)`);

        const previous = g.append("path");
        previous.attr("d", "M7 6c.55 0 1 .45 1 1v10c0 .55-.45 1-1 1s-1-.45-1-1V7c0-.55.45-1 1-1zm3.66 6.82l5.77 4.07c.66.47 1.58-.01 1.58-.82V7.93c0-.81-.91-1.28-1.58-.82l-5.77 4.07c-.57.4-.57 1.24 0 1.64z");

        const next = g.append("path");
        next.attr("d", "M7.58 16.89l5.77-4.07c.56-.4.56-1.24 0-1.63L7.58 7.11C6.91 6.65 6 7.12 6 7.93v8.14c0 .81.91 1.28 1.58.82zM16 7v10c0 .55.45 1 1 1s1-.45 1-1V7c0-.55-.45-1-1-1s-1 .45-1 1z")
            .attr("transform", "translate(30 0)")

        if (links.previous) {
            previous.on("click", () => call(links.previous!!));
        } else {
            previous.attr("fill", "#D0D0D0");
        }

        if (links.next) {
            next.on("click", () => call(links.next!!));
        } else {
            next.attr("fill", "#D0D0D0");
        }

        if (links.play) {
            g.append("path")
                .attr("d", "M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z")
                .attr("transform", "translate(15 0)")
                .on("click", () => call(links.play!!));
        }
        if (links.stop) {
            g.append("path")
                .attr("d", "M8 6h8c1.1 0 2 .9 2 2v8c0 1.1-.9 2-2 2H8c-1.1 0-2-.9-2-2V8c0-1.1.9-2 2-2z")
                .attr("transform", "translate(15 0)")
                .on("click", () => call(links.stop!!));
        }
    }
}

function boundingRectOf(points: Point[]): Rect {
    const xs = points.map((p) => p.x).sort((n1,n2) => n1 - n2);
    const ys = points.map((p) => p.y).sort((n1,n2) => n1 - n2);
    const minX = xs[0];
    const maxX = xs[points.length - 1];

    const minY = ys[0];
    const maxY = ys[points.length - 1];

    return new Rect(minX, minY, maxX - minX, maxY - minY);
}
