import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useFindPlan} from "../../services/PlanService";
import * as d3 from "d3";
import {RoomInformation} from "../../model/RoomInformation";
import {makeRoom} from "./makeRoom";
import {getPlayer, noLinks} from "../../services/PlayerService";
import Player from "./Player";
import {IconButton} from "@material-ui/core";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import {PlayArrowRounded, SkipNext, SkipNextRounded, SkipPreviousRounded, StopRounded} from "@material-ui/icons";

var ReactDOMServer = require('react-dom/server');


const useStyles = makeStyles({
    plan: {
        width: '100%',
        height: '100%',
    },
    image: {
        width: '100%',
        height: '100%',
    },
    root: {
        height: '99vh',
        background: '#EAEAEA',
        paddingTop: '0.5vh',
        paddingBottom: '0.5vh',
    },
    playIcon: {
        height: 38,
        width: 38,
    }
});






const Plan = () => {
    const classes = useStyles();
    const {data: plan} = useFindPlan('home');

    const [roomInformation, setRoomInformation] = useState<RoomInformation[]>([]);

    let ref!: SVGSVGElement;

    useEffect(() => {
        if (!plan)
            return;
        plan.rooms.forEach( (details) => {
            setRoomInformation(o => [...o, new RoomInformation(details)]);
        });

        plan.rooms
            .filter( (room) => room.playerInformationUrl )
            .forEach(
            (room) => {
                getPlayer(room.playerInformationUrl!!).then(({song, links}) => {
                    setRoomInformation(o => [...o.filter((r) => r.room.name !== room.name), new RoomInformation(room, song, links)]);
                }).catch(e => {
                    console.log(e);
                });
                    setInterval(()=>
                        getPlayer(room.playerInformationUrl!!).then(({song, links}) => {
                            setRoomInformation(o => [...o.filter((r) => r.room.name !== room.name), new RoomInformation(room, song, links)]);
                        }).catch(e => {
                            console.log(e);
                        }), 10000);
            }
        );
    }, [plan]);

    useEffect(() => {
        const svgElement = d3.select(ref);
        svgElement.selectAll("polygon").remove();
        svgElement.selectAll("text").remove();
        svgElement.selectAll("g").remove();
        svgElement.selectAll("foreignObject").remove();
        roomInformation.forEach((info) => {

            makeRoom(info, svgElement);
        })
    }, [ref, roomInformation]);
    return (
        <div className={classes.root}>
            <div className={classes.plan}>
                <div className={classes.image}>
                    <svg ref={(r: SVGSVGElement) => ref = r} width='90%' height='90%' viewBox="0 0 142 114">
                        <defs>
                            <filter id="f3" x="0" y="0" width="150%" height="200%">
                                <feOffset result="offOut" in="SourceAlpha" dx="0.5" dy="0.5" />
                                <feGaussianBlur result="blurOut" in="offOut" stdDeviation="0.2" />
                                <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
                                <feComponentTransfer>
                                    <feFuncA type="linear" slope="0.2" />
                                </feComponentTransfer>
                            </filter>
                        </defs>
                    </svg>
                    </div>
            </div>
        </div>
    )
}
/*
<defs>
    <filter id="f3" x="0" y="0" width="150%" height="200%">
        <feOffset result="offOut" in="SourceAlpha" dx="0.5" dy="0.5"></feOffset>
        <feGaussianBlur result="blurOut" in="offOut" stdDeviation="0.2"></feGaussianBlur>
        <feBlend in="SourceGraphic" in2="blurOut" mode="normal"></feBlend>
        <feComponentTransfer>
            <feFuncA type="linear" slope="0.2"></feFuncA>
        </feComponentTransfer>
    </filter>
</defs>
<svg className="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true">
    <path
        d="M7.58 16.89l5.77-4.07c.56-.4.56-1.24 0-1.63L7.58 7.11C6.91 6.65 6 7.12 6 7.93v8.14c0 .81.91 1.28 1.58.82zM16 7v10c0 .55.45 1 1 1s1-.45 1-1V7c0-.55-.45-1-1-1s-1 .45-1 1z"></path>
</svg>
<svg className="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true">
    <path
        d="M7 6c.55 0 1 .45 1 1v10c0 .55-.45 1-1 1s-1-.45-1-1V7c0-.55.45-1 1-1zm3.66 6.82l5.77 4.07c.66.47 1.58-.01 1.58-.82V7.93c0-.81-.91-1.28-1.58-.82l-5.77 4.07c-.57.4-.57 1.24 0 1.64z"></path>
</svg>
<svg className="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true">
    <path
        d="M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z"></path>
</svg>
<svg className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true">
    <path d="M8 6h8c1.1 0 2 .9 2 2v8c0 1.1-.9 2-2 2H8c-1.1 0-2-.9-2-2V8c0-1.1.9-2 2-2z"></path>
</svg>
<polygon points="59 86, 97 86, 97 112, 59 112" fill="white" stroke="#575757" stroke-width="0.5"
         vector-effect="non-scaling-stroke" filter="url(#f3)"></polygon>
<text id="Bathroom" x="63" y="88" width="46" dominant-baseline="hanging" text-anchor="start" font-size="18%"
      fill="darkgray">Bathroom</text>
<text id="Bathroom" x="63" y="99" width="46" dominant-baseline="hanging" text-anchor="start" font-size="18%"
      fill="#202020">Audience with the pope</text>
<text id="Bathroom" x="63" y="103" width="32" dominant-baseline="hanging" text-anchor="start" font-size="18%"
      fill="darkgray">Elbow</text>
<g transform="translate(63 94) scale(0.2 0.2)">
    <path d="M6 6h2v12H6zm3.5 6l8.5 6V6z"></path>
    <path d="M8 5v14l11-7z" transform="translate(15 0)"></path>
    <path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z" transform="translate(30 0)"></path>
</g>*/

export default Plan;


/*
<defs><filter id="f3" x="0" y="0" width="150%" height="200%"><feOffset result="offOut" in="SourceAlpha" dx="0.5" dy="0.5"></feOffset><feGaussianBlur result="blurOut" in="offOut" stdDeviation="0.2"></feGaussianBlur><feBlend in="SourceGraphic" in2="blurOut" mode="normal"></feBlend><feComponentTransfer><feFuncA type="linear" slope="0.2"></feFuncA></feComponentTransfer></filter></defs><svg class="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M7.58 16.89l5.77-4.07c.56-.4.56-1.24 0-1.63L7.58 7.11C6.91 6.65 6 7.12 6 7.93v8.14c0 .81.91 1.28 1.58.82zM16 7v10c0 .55.45 1 1 1s1-.45 1-1V7c0-.55-.45-1-1-1s-1 .45-1 1z"></path></svg><svg class="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M7 6c.55 0 1 .45 1 1v10c0 .55-.45 1-1 1s-1-.45-1-1V7c0-.55.45-1 1-1zm3.66 6.82l5.77 4.07c.66.47 1.58-.01 1.58-.82V7.93c0-.81-.91-1.28-1.58-.82l-5.77 4.07c-.57.4-.57 1.24 0 1.64z"></path></svg><svg class="MuiSvgIcon-root makeStyles-playIcon-47" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z"></path></svg><svg class="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M8 6h8c1.1 0 2 .9 2 2v8c0 1.1-.9 2-2 2H8c-1.1 0-2-.9-2-2V8c0-1.1.9-2 2-2z"></path></svg><polygon points="59 86, 97 86, 97 112, 59 112" fill="white" stroke="#575757" stroke-width="0.5" vector-effect="non-scaling-stroke" filter="url(#f3)"></polygon><text id="Bathroom" x="63" y="88" width="46" dominant-baseline="hanging" text-anchor="start" font-size="18%" fill="darkgray">Bathroom</text><text id="Bathroom" x="63" y="99" width="46" dominant-baseline="hanging" text-anchor="start" font-size="18%" fill="#202020">Audience with the pope</text><text id="Bathroom" x="63" y="103" width="32" dominant-baseline="hanging" text-anchor="start" font-size="18%" fill="darkgray">Elbow</text><g transform="translate(63 94) scale(0.2 0.2)"><path d="M6 6h2v12H6zm3.5 6l8.5 6V6z"></path><path d="M8 5v14l11-7z" transform="translate(15 0)"></path><path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z" transform="translate(30 0)"></path></g>
 */
