import IconButton from "@material-ui/core/IconButton";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import React from "react";
import {createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import PlayerLinks from "../../model/PlayerLinks";
import {ChevronLeft, SkipNext} from "@material-ui/icons";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";

interface PlayerProps {
    links: PlayerLinks;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            width: 400,
            marginBottom: 8,
            marginTop: 8
        },
        details: {
            display: 'flex',
            flexDirection: 'column',
            minWidth: '100%',
        },
        content: {
            flex: '1 0 auto',
            width: '100%',
        },
        cover: {
            width: 151,
        },
        controls: {
            display: 'flex',
            alignItems: 'center',
            padding: theme.spacing(1),
        },
        playIcon: {
            height: 38,
            width: 38,
        },
        roomName: {
            marginLeft: 'auto',
        }
    }),
);

const Player = ({links} : PlayerProps ) => {
    const classes = useStyles();

    return (
        <div style={{width: '120px', height: '24px'}}>
            <SkipPreviousIcon />
            <PlayArrowIcon />
            <SkipNext />
        </div>

        );
}

export default Player;
