import React from 'react';
import {QueryClient, QueryClientProvider} from 'react-query';
import './App.css';
import Plan from "./components/plan/Plan";

function App() {
    const queryClient = new QueryClient();

    return (
        <QueryClientProvider client={queryClient}>
    <div className="App">
        <Plan></Plan>
    </div>
        </QueryClientProvider>
  );
}

export default App;
