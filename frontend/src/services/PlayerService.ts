import PlayerDetails from "../model/PlayerDetails";
import PlayerLinks from "../model/PlayerLinks";

export const noLinks: PlayerLinks = {
    play: null,
    stop: null,

    previous: null,
    next: null
}

export const getPlayer = (url: string) => fetch(url).then(res =>
    {
        if (!res.ok || res.status !== 200) {
            throw  res.status;
        }
        return res.json()
    }
).then(({room, song, state, _links}) => {
        const getLink = (s: string) => (_links && (_links[s] && _links[s].href)) || null;
        const details: PlayerDetails = {
            room: room,
            song: song,
            state: state,
            links: {
                play: getLink('play'),
                stop: getLink('stop'),
                previous: getLink('previous'),
                next: getLink('next'),
            }
        }
        return details;
    }
);
