import {useMutation, useQuery, useQueryClient} from 'react-query';
import PlanDetails from "../model/PlanDetails";

export const useFindPlan = (name: string) => useQuery<PlanDetails>(['plan', name], () => getPlan(name), {retry: false, retryOnMount: false});

export const getPlan = (name: string) => fetch(`http://localhost:8080/flat-plan/${name}`).then(res =>
    {
        if (!res.ok) {
            throw res.status;
        }
        return res.json()
    }
);
